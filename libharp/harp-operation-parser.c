/* A Bison parser, made by GNU Bison 3.7.5.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30705

/* Bison version string.  */
#define YYBISON_VERSION "3.7.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 34 "../harp/libharp/harp-operation-parser.y"


/* *INDENT-ON* */

/* Make parser independent from other parsers */
#define yyerror harp_operation_parser_error
#define yylex   harp_operation_parser_lex
#define yyparse harp_operation_parser_parse

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "harp-operation.h"
#include "harp-program.h"

static harp_program *parsed_program;

/* tokenizer declarations */
int harp_operation_parser_lex(void);
void *harp_operation_parser__scan_string(const char *yy_str);
void harp_operation_parser__delete_buffer(void *);

typedef struct harp_sized_array_struct
{
    harp_data_type data_type;
    int num_elements;
    harp_array array;
} harp_sized_array;

static void harp_operation_parser_error(const char *error)
{
    harp_set_error(HARP_ERROR_OPERATION_SYNTAX, "%s", error);
}

static int parse_int32(const char *buffer, int32_t *dst)
{
    int32_t value = 0;

    while (*buffer != '\0')
    {
        int32_t digit;

        digit = *buffer - '0';
        if (value > (0x7FFFFFFFl - digit) / 10)
        {
            harp_set_error(HARP_ERROR_OPERATION_SYNTAX, "value too large for int32");
            return -1;
        }
        value = 10 * value + digit;
        buffer++;
    }
    *dst = value;

    return 0;
}

int harp_sized_array_new(harp_data_type data_type, harp_sized_array **new_array)
{
    harp_sized_array *sized_array;

    sized_array = (harp_sized_array *)malloc(sizeof(harp_sized_array));
    if (sized_array == NULL)
    {
        harp_set_error(HARP_ERROR_OUT_OF_MEMORY, "out of memory (could not allocate %lu bytes) (%s:%u)",
                       sizeof(harp_sized_array), __FILE__, __LINE__);
        return -1;
    }

    sized_array->data_type = data_type;
    sized_array->num_elements = 0;
    sized_array->array.ptr = NULL;

    *new_array = sized_array;

    return 0;
}

void harp_sized_array_delete(harp_sized_array *sized_array)
{
    if (sized_array->array.ptr != NULL)
    {
        if (sized_array->data_type == harp_type_string)
        {
            long i;

            for (i = 0; i < sized_array->num_elements; i++)
            {
                if (sized_array->array.string_data[i] != NULL)
                {
                    free(sized_array->array.string_data[i]);
                }
            }
        }
        free(sized_array->array.ptr);
    }
    free(sized_array);
}

int harp_sized_array_add_string(harp_sized_array *sized_array, const char *str)
{
    assert(sized_array->data_type == harp_type_string);

    if (sized_array->num_elements % BLOCK_SIZE == 0)
    {
        char **string_data;
        int new_num = (sized_array->num_elements + BLOCK_SIZE);

        string_data = (char **)realloc(sized_array->array.string_data, new_num * sizeof(char *));
        if (string_data == NULL)
        {
            harp_set_error(HARP_ERROR_OUT_OF_MEMORY, "out of memory (could not allocate %lu bytes) (%s:%u)",
                           new_num * sizeof(char *), __FILE__, __LINE__);
            return -1;
        }

        sized_array->array.string_data = string_data;
    }

    sized_array->array.string_data[sized_array->num_elements] = strdup(str);
    sized_array->num_elements++;

    return 0;
}

int harp_sized_array_add_double(harp_sized_array *sized_array, double value)
{
    assert(sized_array->data_type == harp_type_double);

    if (sized_array->num_elements % BLOCK_SIZE == 0)
    {
        double *double_data;
        int new_num = (sized_array->num_elements + BLOCK_SIZE);

        double_data = (double *)realloc(sized_array->array.double_data, new_num * sizeof(double));
        if (double_data == NULL)
        {
            harp_set_error(HARP_ERROR_OUT_OF_MEMORY, "out of memory (could not allocate %lu bytes) (%s:%u)",
                           new_num * sizeof(double), __FILE__, __LINE__);
            return -1;
        }

        sized_array->array.double_data = double_data;
    }

    sized_array->array.double_data[sized_array->num_elements] = value;
    sized_array->num_elements++;

    return 0;
}

int harp_sized_array_add_int32(harp_sized_array *sized_array, int32_t value)
{
    assert(sized_array->data_type == harp_type_int32);

    if (sized_array->num_elements % BLOCK_SIZE == 0)
    {
        int32_t *int32_data;
        int new_num = (sized_array->num_elements + BLOCK_SIZE);

        int32_data = (int32_t *)realloc(sized_array->array.int32_data, new_num * sizeof(int32_t));
        if (int32_data == NULL)
        {
            harp_set_error(HARP_ERROR_OUT_OF_MEMORY, "out of memory (could not allocate %lu bytes) (%s:%u)",
                           new_num * sizeof(int32_t), __FILE__, __LINE__);
            return -1;
        }

        sized_array->array.int32_data = int32_data;
    }

    sized_array->array.int32_data[sized_array->num_elements] = value;
    sized_array->num_elements++;

    return 0;
}

/* *INDENT-OFF* */


#line 256 "libharp/harp-operation-parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_LIBHARP_HARP_OPERATION_PARSER_H_INCLUDED
# define YY_YY_LIBHARP_HARP_OPERATION_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    NAME = 258,                    /* NAME  */
    NAME_PATTERN = 259,            /* NAME_PATTERN  */
    STRING_VALUE = 260,            /* STRING_VALUE  */
    INTEGER_VALUE = 261,           /* INTEGER_VALUE  */
    DOUBLE_VALUE = 262,            /* DOUBLE_VALUE  */
    DATATYPE = 263,                /* DATATYPE  */
    DIMENSION = 264,               /* DIMENSION  */
    UNIT = 265,                    /* UNIT  */
    ID_A = 266,                    /* ID_A  */
    ID_B = 267,                    /* ID_B  */
    FUNC_AREA_COVERS_AREA = 268,   /* FUNC_AREA_COVERS_AREA  */
    FUNC_AREA_COVERS_POINT = 269,  /* FUNC_AREA_COVERS_POINT  */
    FUNC_AREA_INSIDE_AREA = 270,   /* FUNC_AREA_INSIDE_AREA  */
    FUNC_AREA_INTERSECTS_AREA = 271, /* FUNC_AREA_INTERSECTS_AREA  */
    FUNC_BIN = 272,                /* FUNC_BIN  */
    FUNC_BIN_SPATIAL = 273,        /* FUNC_BIN_SPATIAL  */
    FUNC_CLAMP = 274,              /* FUNC_CLAMP  */
    FUNC_COLLOCATE_LEFT = 275,     /* FUNC_COLLOCATE_LEFT  */
    FUNC_COLLOCATE_RIGHT = 276,    /* FUNC_COLLOCATE_RIGHT  */
    FUNC_DERIVE = 277,             /* FUNC_DERIVE  */
    FUNC_DERIVE_SMOOTHED_COLUMN = 278, /* FUNC_DERIVE_SMOOTHED_COLUMN  */
    FUNC_EXCLUDE = 279,            /* FUNC_EXCLUDE  */
    FUNC_FLATTEN = 280,            /* FUNC_FLATTEN  */
    FUNC_INDEX = 281,              /* FUNC_INDEX  */
    FUNC_KEEP = 282,               /* FUNC_KEEP  */
    FUNC_LONGITUDE_RANGE = 283,    /* FUNC_LONGITUDE_RANGE  */
    FUNC_POINT_DISTANCE = 284,     /* FUNC_POINT_DISTANCE  */
    FUNC_POINT_IN_AREA = 285,      /* FUNC_POINT_IN_AREA  */
    FUNC_REBIN = 286,              /* FUNC_REBIN  */
    FUNC_REGRID = 287,             /* FUNC_REGRID  */
    FUNC_RENAME = 288,             /* FUNC_RENAME  */
    FUNC_SET = 289,                /* FUNC_SET  */
    FUNC_SMOOTH = 290,             /* FUNC_SMOOTH  */
    FUNC_SORT = 291,               /* FUNC_SORT  */
    FUNC_SQUASH = 292,             /* FUNC_SQUASH  */
    FUNC_VALID = 293,              /* FUNC_VALID  */
    FUNC_WRAP = 294,               /* FUNC_WRAP  */
    NAN = 295,                     /* NAN  */
    INF = 296,                     /* INF  */
    IN = 297,                      /* IN  */
    EQUAL = 298,                   /* EQUAL  */
    NOT_EQUAL = 299,               /* NOT_EQUAL  */
    GREATER_EQUAL = 300,           /* GREATER_EQUAL  */
    LESS_EQUAL = 301,              /* LESS_EQUAL  */
    BIT_NAND = 302,                /* BIT_NAND  */
    BIT_AND = 303,                 /* BIT_AND  */
    NOT = 304                      /* NOT  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define NAME 258
#define NAME_PATTERN 259
#define STRING_VALUE 260
#define INTEGER_VALUE 261
#define DOUBLE_VALUE 262
#define DATATYPE 263
#define DIMENSION 264
#define UNIT 265
#define ID_A 266
#define ID_B 267
#define FUNC_AREA_COVERS_AREA 268
#define FUNC_AREA_COVERS_POINT 269
#define FUNC_AREA_INSIDE_AREA 270
#define FUNC_AREA_INTERSECTS_AREA 271
#define FUNC_BIN 272
#define FUNC_BIN_SPATIAL 273
#define FUNC_CLAMP 274
#define FUNC_COLLOCATE_LEFT 275
#define FUNC_COLLOCATE_RIGHT 276
#define FUNC_DERIVE 277
#define FUNC_DERIVE_SMOOTHED_COLUMN 278
#define FUNC_EXCLUDE 279
#define FUNC_FLATTEN 280
#define FUNC_INDEX 281
#define FUNC_KEEP 282
#define FUNC_LONGITUDE_RANGE 283
#define FUNC_POINT_DISTANCE 284
#define FUNC_POINT_IN_AREA 285
#define FUNC_REBIN 286
#define FUNC_REGRID 287
#define FUNC_RENAME 288
#define FUNC_SET 289
#define FUNC_SMOOTH 290
#define FUNC_SORT 291
#define FUNC_SQUASH 292
#define FUNC_VALID 293
#define FUNC_WRAP 294
#define NAN 295
#define INF 296
#define IN 297
#define EQUAL 298
#define NOT_EQUAL 299
#define GREATER_EQUAL 300
#define LESS_EQUAL 301
#define BIT_NAND 302
#define BIT_AND 303
#define NOT 304

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 219 "../harp/libharp/harp-operation-parser.y"

    double double_val;
    int32_t int32_val;
    char *string_val;
    const char *const_string_val;
    harp_operation *operation;
    harp_sized_array *array;
    harp_program *program;

    harp_comparison_operator_type comparison_operator;
    harp_bit_mask_operator_type bit_mask_operator;
    harp_membership_operator_type membership_operator;

#line 421 "libharp/harp-operation-parser.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_LIBHARP_HARP_OPERATION_PARSER_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_NAME = 3,                       /* NAME  */
  YYSYMBOL_NAME_PATTERN = 4,               /* NAME_PATTERN  */
  YYSYMBOL_STRING_VALUE = 5,               /* STRING_VALUE  */
  YYSYMBOL_INTEGER_VALUE = 6,              /* INTEGER_VALUE  */
  YYSYMBOL_DOUBLE_VALUE = 7,               /* DOUBLE_VALUE  */
  YYSYMBOL_DATATYPE = 8,                   /* DATATYPE  */
  YYSYMBOL_DIMENSION = 9,                  /* DIMENSION  */
  YYSYMBOL_UNIT = 10,                      /* UNIT  */
  YYSYMBOL_ID_A = 11,                      /* ID_A  */
  YYSYMBOL_ID_B = 12,                      /* ID_B  */
  YYSYMBOL_FUNC_AREA_COVERS_AREA = 13,     /* FUNC_AREA_COVERS_AREA  */
  YYSYMBOL_FUNC_AREA_COVERS_POINT = 14,    /* FUNC_AREA_COVERS_POINT  */
  YYSYMBOL_FUNC_AREA_INSIDE_AREA = 15,     /* FUNC_AREA_INSIDE_AREA  */
  YYSYMBOL_FUNC_AREA_INTERSECTS_AREA = 16, /* FUNC_AREA_INTERSECTS_AREA  */
  YYSYMBOL_FUNC_BIN = 17,                  /* FUNC_BIN  */
  YYSYMBOL_FUNC_BIN_SPATIAL = 18,          /* FUNC_BIN_SPATIAL  */
  YYSYMBOL_FUNC_CLAMP = 19,                /* FUNC_CLAMP  */
  YYSYMBOL_FUNC_COLLOCATE_LEFT = 20,       /* FUNC_COLLOCATE_LEFT  */
  YYSYMBOL_FUNC_COLLOCATE_RIGHT = 21,      /* FUNC_COLLOCATE_RIGHT  */
  YYSYMBOL_FUNC_DERIVE = 22,               /* FUNC_DERIVE  */
  YYSYMBOL_FUNC_DERIVE_SMOOTHED_COLUMN = 23, /* FUNC_DERIVE_SMOOTHED_COLUMN  */
  YYSYMBOL_FUNC_EXCLUDE = 24,              /* FUNC_EXCLUDE  */
  YYSYMBOL_FUNC_FLATTEN = 25,              /* FUNC_FLATTEN  */
  YYSYMBOL_FUNC_INDEX = 26,                /* FUNC_INDEX  */
  YYSYMBOL_FUNC_KEEP = 27,                 /* FUNC_KEEP  */
  YYSYMBOL_FUNC_LONGITUDE_RANGE = 28,      /* FUNC_LONGITUDE_RANGE  */
  YYSYMBOL_FUNC_POINT_DISTANCE = 29,       /* FUNC_POINT_DISTANCE  */
  YYSYMBOL_FUNC_POINT_IN_AREA = 30,        /* FUNC_POINT_IN_AREA  */
  YYSYMBOL_FUNC_REBIN = 31,                /* FUNC_REBIN  */
  YYSYMBOL_FUNC_REGRID = 32,               /* FUNC_REGRID  */
  YYSYMBOL_FUNC_RENAME = 33,               /* FUNC_RENAME  */
  YYSYMBOL_FUNC_SET = 34,                  /* FUNC_SET  */
  YYSYMBOL_FUNC_SMOOTH = 35,               /* FUNC_SMOOTH  */
  YYSYMBOL_FUNC_SORT = 36,                 /* FUNC_SORT  */
  YYSYMBOL_FUNC_SQUASH = 37,               /* FUNC_SQUASH  */
  YYSYMBOL_FUNC_VALID = 38,                /* FUNC_VALID  */
  YYSYMBOL_FUNC_WRAP = 39,                 /* FUNC_WRAP  */
  YYSYMBOL_NAN = 40,                       /* NAN  */
  YYSYMBOL_INF = 41,                       /* INF  */
  YYSYMBOL_IN = 42,                        /* IN  */
  YYSYMBOL_EQUAL = 43,                     /* EQUAL  */
  YYSYMBOL_NOT_EQUAL = 44,                 /* NOT_EQUAL  */
  YYSYMBOL_GREATER_EQUAL = 45,             /* GREATER_EQUAL  */
  YYSYMBOL_LESS_EQUAL = 46,                /* LESS_EQUAL  */
  YYSYMBOL_BIT_NAND = 47,                  /* BIT_NAND  */
  YYSYMBOL_BIT_AND = 48,                   /* BIT_AND  */
  YYSYMBOL_NOT = 49,                       /* NOT  */
  YYSYMBOL_50_ = 50,                       /* ';'  */
  YYSYMBOL_51_ = 51,                       /* '+'  */
  YYSYMBOL_52_ = 52,                       /* '-'  */
  YYSYMBOL_53_ = 53,                       /* ','  */
  YYSYMBOL_54_ = 54,                       /* '{'  */
  YYSYMBOL_55_ = 55,                       /* '}'  */
  YYSYMBOL_56_ = 56,                       /* '>'  */
  YYSYMBOL_57_ = 57,                       /* '<'  */
  YYSYMBOL_58_ = 58,                       /* '('  */
  YYSYMBOL_59_ = 59,                       /* ')'  */
  YYSYMBOL_YYACCEPT = 60,                  /* $accept  */
  YYSYMBOL_input = 61,                     /* input  */
  YYSYMBOL_reserved_identifier = 62,       /* reserved_identifier  */
  YYSYMBOL_identifier = 63,                /* identifier  */
  YYSYMBOL_identifier_pattern = 64,        /* identifier_pattern  */
  YYSYMBOL_double_value = 65,              /* double_value  */
  YYSYMBOL_int32_value = 66,               /* int32_value  */
  YYSYMBOL_double_array = 67,              /* double_array  */
  YYSYMBOL_int32_array = 68,               /* int32_array  */
  YYSYMBOL_string_array = 69,              /* string_array  */
  YYSYMBOL_identifier_array = 70,          /* identifier_array  */
  YYSYMBOL_identifier_pattern_array = 71,  /* identifier_pattern_array  */
  YYSYMBOL_dimension_array = 72,           /* dimension_array  */
  YYSYMBOL_dimensionspec = 73,             /* dimensionspec  */
  YYSYMBOL_comparison_operator = 74,       /* comparison_operator  */
  YYSYMBOL_bit_mask_operator = 75,         /* bit_mask_operator  */
  YYSYMBOL_membership_operator = 76,       /* membership_operator  */
  YYSYMBOL_operation = 77,                 /* operation  */
  YYSYMBOL_program = 78                    /* program  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  69
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   763

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  60
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  19
/* YYNRULES -- Number of rules.  */
#define YYNRULES  182
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  538

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   304


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      58,    59,     2,    51,    53,    52,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    50,
      57,     2,    56,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    54,     2,    55,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   298,   298,   299,   303,   304,   305,   306,   307,   308,
     309,   310,   311,   312,   313,   314,   315,   316,   317,   318,
     319,   320,   321,   322,   323,   324,   325,   326,   327,   328,
     329,   330,   331,   332,   333,   334,   335,   336,   337,   341,
     342,   346,   347,   351,   360,   369,   379,   380,   381,   382,
     383,   384,   388,   396,   404,   416,   420,   427,   431,   438,
     448,   465,   475,   492,   502,   519,   527,   538,   539,   543,
     544,   545,   546,   547,   548,   552,   553,   557,   558,   562,
     570,   578,   588,   598,   610,   620,   631,   637,   646,   657,
     670,   683,   698,   706,   709,   717,   725,   735,   746,   759,
     772,   787,   795,   807,   821,   835,   851,   859,   873,   889,
     905,   923,   934,   940,   948,   956,   964,   972,   983,  1025,
    1035,  1043,  1051,  1059,  1067,  1075,  1083,  1094,  1104,  1114,
    1126,  1137,  1150,  1163,  1178,  1200,  1222,  1241,  1249,  1252,
    1260,  1263,  1271,  1279,  1289,  1292,  1300,  1308,  1318,  1326,
    1336,  1346,  1358,  1369,  1382,  1395,  1410,  1418,  1430,  1461,
    1473,  1488,  1519,  1533,  1547,  1559,  1569,  1579,  1596,  1613,
    1627,  1645,  1663,  1678,  1686,  1694,  1702,  1710,  1718,  1726,
    1739,  1748,  1761
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "NAME", "NAME_PATTERN",
  "STRING_VALUE", "INTEGER_VALUE", "DOUBLE_VALUE", "DATATYPE", "DIMENSION",
  "UNIT", "ID_A", "ID_B", "FUNC_AREA_COVERS_AREA",
  "FUNC_AREA_COVERS_POINT", "FUNC_AREA_INSIDE_AREA",
  "FUNC_AREA_INTERSECTS_AREA", "FUNC_BIN", "FUNC_BIN_SPATIAL",
  "FUNC_CLAMP", "FUNC_COLLOCATE_LEFT", "FUNC_COLLOCATE_RIGHT",
  "FUNC_DERIVE", "FUNC_DERIVE_SMOOTHED_COLUMN", "FUNC_EXCLUDE",
  "FUNC_FLATTEN", "FUNC_INDEX", "FUNC_KEEP", "FUNC_LONGITUDE_RANGE",
  "FUNC_POINT_DISTANCE", "FUNC_POINT_IN_AREA", "FUNC_REBIN", "FUNC_REGRID",
  "FUNC_RENAME", "FUNC_SET", "FUNC_SMOOTH", "FUNC_SORT", "FUNC_SQUASH",
  "FUNC_VALID", "FUNC_WRAP", "NAN", "INF", "IN", "EQUAL", "NOT_EQUAL",
  "GREATER_EQUAL", "LESS_EQUAL", "BIT_NAND", "BIT_AND", "NOT", "';'",
  "'+'", "'-'", "','", "'{'", "'}'", "'>'", "'<'", "'('", "')'", "$accept",
  "input", "reserved_identifier", "identifier", "identifier_pattern",
  "double_value", "int32_value", "double_array", "int32_array",
  "string_array", "identifier_array", "identifier_pattern_array",
  "dimension_array", "dimensionspec", "comparison_operator",
  "bit_mask_operator", "membership_operator", "operation", "program", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
      59,    43,    45,    44,   123,   125,    62,    60,    40,    41
};
#endif

#define YYPACT_NINF (-152)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     629,  -152,  -152,  -152,  -152,  -152,   -49,   -37,   -26,   -18,
     -16,   -14,    51,    69,    86,   101,   104,   122,   127,   177,
     179,   182,   187,   209,   229,   232,   253,   267,   268,   269,
     272,   273,   275,  -152,  -152,  -152,  -152,   105,  -152,   160,
    -152,    57,     0,   115,     7,    11,   439,    28,   166,   172,
     214,   671,   671,   587,   238,   249,   587,   115,   115,    12,
     305,   327,   671,   237,   329,   491,   364,   671,   671,  -152,
    -152,  -152,  -152,  -152,  -152,  -152,  -152,   196,  -152,  -152,
     106,   117,   316,   629,   318,   115,  -152,  -152,  -152,   184,
     190,   100,  -152,   320,   115,   -40,   115,   322,  -152,  -152,
    -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,
    -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,  -152,
    -152,  -152,  -152,  -152,  -152,   671,  -152,   321,   375,   376,
     115,   330,   331,   -33,   -30,    14,   332,  -152,  -152,  -152,
     -28,   326,   334,   -20,   118,   119,   335,   115,   336,   337,
     342,   343,   671,   344,   671,   339,   346,   345,   120,  -152,
    -152,   391,  -152,   109,  -152,  -152,  -152,    65,  -152,  -152,
    -152,  -152,  -152,  -152,  -152,  -152,   349,   115,  -152,    66,
     115,  -152,    67,   189,  -152,    88,  -152,    89,   115,   671,
     117,  -152,   117,  -152,    31,   347,    95,  -152,     8,   393,
     587,  -152,  -152,   207,  -152,   354,   115,   355,   115,  -152,
     111,   671,   671,   671,   404,   134,   401,   135,  -152,   539,
    -152,   358,   115,  -152,  -152,   136,   139,   115,   121,   115,
      17,   123,   353,   125,   356,   360,   671,   361,   368,   369,
     403,   161,   173,   365,  -152,    25,  -152,  -152,  -152,   -25,
     366,  -152,   370,  -152,   117,   359,   115,    33,   115,   128,
     129,   416,   417,   372,   373,   380,   381,   377,   671,   378,
     115,   385,   418,   409,  -152,  -152,   386,   371,    37,   384,
    -152,   387,   424,  -152,   392,   425,  -152,  -152,  -152,  -152,
     426,   115,   396,   117,  -152,   117,  -152,  -152,   427,  -152,
     476,  -152,  -152,   671,  -152,   117,    39,   428,  -152,   130,
     436,   115,   437,   433,   440,   442,  -152,  -152,   483,   671,
    -152,   175,  -152,   443,   115,  -152,  -152,   477,   115,   475,
    -152,  -152,   478,   115,   479,   115,   115,   448,   480,   482,
     484,  -152,  -152,   529,  -152,   180,   485,  -152,  -152,   492,
     115,   115,    40,   488,   115,    30,     2,   530,   572,   525,
     115,   526,   115,   201,  -152,   115,   202,   115,   212,   213,
     117,   115,  -152,  -152,   533,   117,  -152,  -152,   115,    41,
      42,   528,  -152,   115,   221,   115,   536,   223,   115,   540,
     671,   541,  -152,   571,  -152,   224,    47,   225,    49,   226,
      36,   574,   578,   581,   630,  -152,    73,   580,  -152,   613,
    -152,  -152,   233,    77,   235,   115,   200,  -152,   236,   115,
     582,   668,  -152,    84,   616,  -152,    92,   617,  -152,    38,
     244,   115,  -152,  -152,   115,   115,   245,   618,  -152,  -152,
    -152,    93,   622,  -152,   655,   662,   663,   664,   246,   665,
     666,   247,   667,  -152,  -152,   669,  -152,  -152,   256,   115,
    -152,   115,  -152,   670,   672,   673,   290,  -152,  -152,   674,
    -152,  -152,  -152,   115,   716,   717,   676,  -152,   115,   718,
     310,  -152,  -152,  -152,   115,  -152,   677,   678,  -152,   115,
     679,   682,   686,  -152,   681,   683,   684,   115,   685,   257,
     688,   692,   687,  -152,  -152,   689,  -152,   719,   722,  -152,
    -152,  -152,   259,  -152,   312,  -152,   725,   726,  -152,  -152,
     690,   691,   693,   694,   698,   695,   696,  -152,  -152,  -152,
     748,   751,  -152,  -152,   699,   700,  -152,  -152
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
     182,    39,     9,     8,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,     4,     5,     7,     6,     0,    40,     0,
     181,     3,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     1,
      78,    69,    70,    71,    72,    75,    76,     0,    73,    74,
       0,     0,     0,     2,     0,     0,    52,    43,    47,     0,
       0,     0,    46,     0,     0,     0,     0,     0,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,     0,   112,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    41,    42,    64,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    77,
      82,    80,    79,     0,   180,    92,    56,     0,    53,    44,
      48,    50,    54,    45,    49,    51,     0,     0,   101,     0,
       0,   106,     0,     0,    62,     0,   113,     0,     0,     0,
       0,   120,     0,   123,     0,     0,     0,   126,     0,     0,
       0,   137,   138,     0,   139,     0,     0,     0,     0,   156,
       0,     0,     0,     0,     0,     0,     0,     0,   173,     0,
     177,     0,     0,    81,    60,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   128,     0,   127,    66,    68,     0,
       0,   130,     0,    63,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    84,     0,    85,    55,     0,     0,     0,     0,
      93,     0,     0,   111,     0,     0,   115,   116,    61,   114,
       0,     0,     0,     0,   121,     0,   124,   129,     0,   132,
       0,    67,   131,     0,    86,     0,     0,     0,   140,     0,
       0,     0,     0,     0,     0,     0,   165,   166,     0,     0,
     174,     0,   175,     0,     0,    83,    59,     0,     0,     0,
      95,    94,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   133,    65,     0,    58,     0,     0,   142,   141,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    96,     0,     0,     0,     0,     0,
       0,     0,   122,   125,     0,     0,    87,   143,     0,     0,
       0,     0,   144,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   176,     0,   178,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    57,     0,     0,   148,     0,
     146,   145,     0,     0,     0,     0,     0,   164,     0,     0,
       0,     0,   179,     0,     0,    88,     0,     0,    97,     0,
       0,     0,   102,   117,     0,     0,     0,     0,   150,   149,
     147,     0,     0,   152,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    90,    89,     0,    99,    98,     0,     0,
     104,     0,   103,     0,     0,     0,     0,   136,   151,     0,
     154,   153,   157,     0,     0,     0,     0,   159,     0,     0,
       0,   169,    91,   100,     0,   105,     0,     0,   107,     0,
       0,     0,     0,   155,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   109,   108,     0,   119,     0,     0,   158,
     162,   163,     0,   161,     0,   172,     0,     0,   110,   118,
       0,     0,     0,     0,     0,     0,     0,   134,   135,   160,
       0,     0,   167,   168,     0,     0,   170,   171
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -152,  -152,  -152,    10,   557,   -43,   -36,   -92,  -152,  -152,
    -151,   704,  -152,  -130,   558,  -152,   559,   680,  -152
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
       0,    37,    38,   184,   139,   166,    92,   167,   345,   226,
     185,   140,   249,   198,    80,    81,    82,    40,    41
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      91,   215,   179,   217,   182,    84,   199,   387,    86,    42,
      39,   131,    93,   180,   144,   145,    95,   146,   250,   181,
     190,    43,   194,   192,   195,   200,   191,   279,   300,   193,
     301,   201,    44,   200,    86,   298,    86,   161,   187,   204,
      45,   243,    46,   307,    47,   162,   430,   329,   458,   346,
     381,   407,   409,   128,   129,   210,   127,   424,    85,   427,
     388,   135,   136,   138,   245,    94,   138,   251,   196,    96,
     147,   225,   150,   197,   153,   155,   280,   157,   158,   128,
     129,   128,   129,   437,   299,   196,   130,   442,   385,   431,
     244,   459,   308,    39,   452,   432,   330,   460,   347,   382,
     408,   410,   455,   469,   247,    69,   425,    83,   428,    48,
     176,   160,    86,    87,   224,    86,    87,   321,   227,   227,
     227,    86,    87,    86,   228,   231,   233,    49,   205,   207,
     221,   276,   438,   281,   230,   284,   443,   232,   310,   312,
     349,   236,   227,   453,    50,   239,    88,   237,   238,    88,
     248,   456,   470,   177,   241,    88,   242,    89,    90,    51,
      89,    90,    52,   257,   227,   259,    89,    90,   128,   129,
     260,   206,   208,   222,   277,   132,   282,   133,   285,   271,
      53,   311,   313,   350,   275,    54,   278,   236,   236,   227,
     168,   169,   273,   265,   267,   272,   172,   173,   274,   240,
     234,   235,    70,    71,    72,    73,    74,    75,    76,    77,
     138,   446,   447,   306,   293,   309,    78,    79,   304,   134,
     294,   261,   262,   263,   170,   171,   295,   323,   236,   269,
     174,   175,   296,   375,   359,    55,   363,    56,   159,   376,
      57,   366,   151,   368,   369,    58,   288,   141,   337,    70,
      71,    72,    73,    74,   227,   227,    77,   339,   142,   340,
     396,   398,   384,    78,    79,   227,   227,    59,   352,   344,
     395,   400,   401,   397,   227,   399,   416,   227,   227,   227,
     413,   361,   417,   423,   426,   429,   227,    60,   227,   227,
      61,   412,   441,   414,   444,   448,   418,   461,   466,   476,
     480,   491,   492,   462,   467,   477,   481,   379,   380,   484,
     514,    62,   227,   343,   148,   485,   515,   393,   522,   386,
     389,   500,   501,   523,   524,    63,    64,    65,   403,   358,
      66,    67,     1,    68,   402,   406,   149,     2,     3,   405,
       4,     5,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,    33,
      34,    35,   445,   156,   163,   183,   449,   165,    36,   178,
     186,   168,   172,   188,   189,   202,   196,   152,   463,   211,
     212,   464,   465,   203,   209,   213,   214,   216,   218,   219,
     420,   223,   229,   252,   220,   512,   246,   256,   258,   264,
     266,   270,   283,   292,   326,   286,   486,   305,   487,   287,
     289,   290,   291,   303,   297,   302,   314,   315,   325,   328,
     494,   316,   317,   318,   319,   498,   320,   322,   324,   327,
     332,   502,     1,   331,    97,   334,   505,     2,     3,   338,
       4,     5,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,    33,
      34,    35,   333,   335,   336,   342,   341,   348,    36,   351,
     353,   354,   357,   355,     1,   356,   360,   125,   126,     2,
       3,   370,     4,     5,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,    33,    34,    35,   364,   362,   365,   367,   371,   374,
      36,   372,     1,   373,   377,   378,   383,     2,     3,   154,
       4,     5,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,    33,
      34,    35,   391,   390,   392,   394,   404,   411,    36,   415,
       1,   137,   450,   419,   421,     2,     3,   268,     4,     5,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,    33,    34,    35,
     422,   434,     1,   433,   435,   436,    36,     2,     3,   439,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,   440,   451,     1,   454,   457,   468,    36,     2,
       3,   471,     4,     5,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,    33,    34,    35,   472,   473,   474,   475,   478,   479,
      36,   495,   496,   499,   520,   489,   482,   521,   483,   488,
     525,   526,   490,   493,   497,   507,   503,   504,   506,   508,
     509,   516,   510,   511,   513,   517,   518,   530,   519,   527,
     528,   531,   529,   534,   532,   533,   535,   253,   536,   537,
     143,   254,   255,   164
};

static const yytype_int16 yycheck[] =
{
      43,   152,    94,   154,    96,     5,   136,     5,     6,    58,
       0,    47,     5,    53,    57,    58,     5,     5,    10,    59,
      53,    58,     8,    53,    10,    53,    59,    10,    53,    59,
      55,    59,    58,    53,     6,    10,     6,    80,   130,    59,
      58,    10,    58,    10,    58,    81,    10,    10,    10,    10,
      10,    10,    10,    51,    52,   147,    46,    10,    58,    10,
      58,    51,    52,    53,   194,    58,    56,    59,    54,    58,
      58,   163,    62,    59,    64,    65,    59,    67,    68,    51,
      52,    51,    52,    10,    59,    54,    58,    10,    58,    53,
      59,    53,    59,    83,    10,    59,    59,    59,    59,    59,
      59,    59,    10,    10,     9,     0,    59,    50,    59,    58,
      10,     5,     6,     7,     5,     6,     7,   268,    53,    53,
      53,     6,     7,     6,    59,    59,    59,    58,    10,    10,
      10,    10,    59,    10,   177,    10,    59,   180,    10,    10,
      10,    53,    53,    59,    58,   188,    40,    59,    59,    40,
      55,    59,    59,    53,   190,    40,   192,    51,    52,    58,
      51,    52,    58,   206,    53,   208,    51,    52,    51,    52,
      59,    53,    53,    53,    53,     9,    53,     5,    53,   222,
      58,    53,    53,    53,   227,    58,   229,    53,    53,    53,
       6,     7,    53,    59,    59,    59,     6,     7,    59,   189,
      11,    12,    42,    43,    44,    45,    46,    47,    48,    49,
     200,    11,    12,   256,    53,   258,    56,    57,   254,     5,
      59,   211,   212,   213,    40,    41,    53,   270,    53,   219,
      40,    41,    59,    53,    59,    58,   328,    58,    42,    59,
      58,   333,     5,   335,   336,    58,   236,     9,   291,    42,
      43,    44,    45,    46,    53,    53,    49,   293,     9,   295,
      59,    59,   354,    56,    57,    53,    53,    58,   311,   305,
     362,    59,    59,   365,    53,   367,    53,    53,    53,    53,
      59,   324,    59,    59,    59,    59,    53,    58,    53,    53,
      58,   383,    59,   385,    59,    59,   388,    53,    53,    53,
      53,    11,    12,    59,    59,    59,    59,   350,   351,    53,
      53,    58,    53,   303,     9,    59,    59,   360,    59,   355,
     356,    11,    12,    11,    12,    58,    58,    58,   371,   319,
      58,    58,     3,    58,   370,   378,     9,     8,     9,   375,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,   415,     9,    58,    53,   419,    59,    49,    59,
      59,     6,     6,    53,    53,    59,    54,    58,   431,    53,
      53,   434,   435,    59,    59,    53,    53,    53,    59,    53,
     390,    10,    53,    10,    59,   497,    59,    53,    53,     5,
       9,    53,    59,    10,     5,    59,   459,    58,   461,    59,
      59,    53,    53,    53,    59,    59,    10,    10,    10,    58,
     473,    59,    59,    53,    53,   478,    59,    59,    53,    53,
      53,   484,     3,    59,     5,    53,   489,     8,     9,    53,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    58,    58,    58,     9,    59,    59,    49,    53,
      53,    58,     9,    53,     3,    53,    53,    58,    59,     8,
       9,    53,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    59,    58,    58,    58,    58,    10,
      49,    59,     3,    59,    59,    53,    58,     8,     9,    58,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    10,    53,    59,    59,    53,    59,    49,    53,
       3,     4,    10,    53,    53,     8,     9,    58,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      59,    53,     3,    59,    53,     5,    49,     8,     9,    59,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    59,     5,     3,    59,    59,    59,    49,     8,
       9,    59,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    59,    53,    53,    53,    53,    53,
      49,     5,     5,     5,     5,    53,    59,     5,    59,    59,
       5,     5,    59,    59,    58,    53,    59,    59,    59,    53,
      59,    53,    59,    59,    59,    53,    59,    53,    59,    59,
      59,    53,    59,     5,    59,    59,     5,   200,    59,    59,
      56,   203,   203,    83
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     8,     9,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    49,    61,    62,    63,
      77,    78,    58,    58,    58,    58,    58,    58,    58,    58,
      58,    58,    58,    58,    58,    58,    58,    58,    58,    58,
      58,    58,    58,    58,    58,    58,    58,    58,    58,     0,
      42,    43,    44,    45,    46,    47,    48,    49,    56,    57,
      74,    75,    76,    50,     5,    58,     6,     7,    40,    51,
      52,    65,    66,     5,    58,     5,    58,     5,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    58,    59,    63,    51,    52,
      58,    66,     9,     5,     5,    63,    63,     4,    63,    64,
      71,     9,     9,    71,    65,    65,     5,    58,     9,     9,
      63,     5,    58,    63,    58,    63,     9,    63,    63,    42,
       5,    65,    66,    58,    77,    59,    65,    67,     6,     7,
      40,    41,     6,     7,    40,    41,    10,    53,    59,    67,
      53,    59,    67,    53,    63,    70,    59,    67,    53,    53,
      53,    59,    53,    59,     8,    10,    54,    59,    73,    73,
      53,    59,    59,    59,    59,    10,    53,    10,    53,    59,
      67,    53,    53,    53,    53,    70,    53,    70,    59,    53,
      59,    10,    53,    10,     5,    67,    69,    53,    59,    53,
      65,    59,    65,    59,    11,    12,    53,    59,    59,    65,
      63,    66,    66,    10,    59,    73,    59,     9,    55,    72,
      10,    59,    10,    64,    74,    76,    53,    65,    53,    65,
      59,    63,    63,    63,     5,    59,     9,    59,    58,    63,
      53,    65,    59,    53,    59,    65,    10,    53,    65,    10,
      59,    10,    53,    59,    10,    53,    59,    59,    63,    59,
      53,    53,    10,    53,    59,    53,    59,    59,    10,    59,
      53,    55,    59,    53,    66,    58,    65,    10,    59,    65,
      10,    53,    10,    53,    10,    10,    59,    59,    53,    53,
      59,    70,    59,    65,    53,    10,     5,    53,    58,    10,
      59,    59,    53,    58,    53,    58,    58,    65,    53,    66,
      66,    59,     9,    63,    66,    68,    10,    59,    59,    10,
      53,    53,    65,    53,    58,    53,    53,     9,    63,    59,
      53,    65,    58,    67,    59,    58,    67,    58,    67,    67,
      53,    58,    59,    59,    10,    53,    59,    59,    53,    65,
      65,    10,    59,    58,    67,    58,    66,     5,    58,    66,
      53,    10,    59,    65,    59,    67,    59,    67,    59,    67,
      59,    59,    66,    65,    53,    66,    65,    10,    59,    10,
      59,    59,    67,    59,    67,    53,    53,    59,    67,    53,
      63,    53,    59,    59,    10,    59,    59,    10,    59,    59,
      10,    53,    59,    59,    53,    53,     5,    10,    59,    59,
      59,    59,    10,    59,    59,    65,    11,    12,    59,    65,
      10,     5,    10,    59,    59,    10,    59,    59,    10,    53,
      59,    53,    59,    65,    65,    65,    53,    59,    59,    10,
      59,    59,    59,    53,    53,    53,    53,    59,    53,    53,
      53,    59,    59,    59,    53,    59,    65,    65,    59,    53,
      59,    11,    12,    59,    65,     5,     5,    58,    65,     5,
      11,    12,    65,    59,    59,    65,    59,    53,    53,    59,
      59,    59,    67,    59,    53,    59,    53,    53,    59,    59,
       5,     5,    59,    11,    12,     5,     5,    59,    59,    59,
      53,    53,    59,    59,     5,     5,    59,    59
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    60,    61,    61,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    63,
      63,    64,    64,    65,    65,    65,    65,    65,    65,    65,
      65,    65,    66,    66,    66,    67,    67,    68,    68,    69,
      69,    70,    70,    71,    71,    72,    72,    73,    73,    74,
      74,    74,    74,    74,    74,    75,    75,    76,    76,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      78,    78,    78
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     1,     1,     2,     2,
       2,     2,     1,     2,     2,     3,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     1,     3,
       3,     4,     3,     6,     5,     5,     6,     8,    10,    11,
      11,    12,     4,     6,     7,     7,     8,    10,    11,    11,
      12,     4,    10,    11,    11,    12,     4,    12,    13,    13,
      14,     6,     3,     4,     6,     6,     6,    10,    14,    13,
       4,     6,     8,     4,     6,     8,     4,     5,     5,     6,
       5,     6,     6,     7,    15,    15,    11,     4,     4,     4,
       6,     7,     7,     8,     8,     9,     9,    10,     9,    10,
      10,    11,    10,    11,    11,    12,     4,    11,    13,    11,
      15,    13,    13,    13,     9,     6,     6,    15,    15,    11,
      17,    17,    13,     4,     6,     6,     8,     4,     8,     9,
       3,     1,     0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
# ifndef YY_LOCATION_PRINT
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yysymbol_kind_t yytoken;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  int yyn = yypact[+*yyctx->yyssp];
  if (!yypact_value_is_default (yyn))
    {
      /* Start YYX at -YYN if negative to avoid negative indexes in
         YYCHECK.  In other words, skip the first -YYN actions for
         this state because they are default actions.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;
      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yyx;
      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
        if (yycheck[yyx + yyn] == yyx && yyx != YYSYMBOL_YYerror
            && !yytable_value_is_error (yytable[yyx + yyn]))
          {
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = YY_CAST (yysymbol_kind_t, yyx);
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




#ifndef yystrlen
# if defined __GLIBC__ && defined _STRING_H
#  define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
# endif
#endif

#ifndef yystpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define yystpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
# endif
#endif

#ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;
      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
#endif


static int
yy_syntax_error_arguments (const yypcontext_t *yyctx,
                           yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yyctx->yytoken != YYSYMBOL_YYEMPTY)
    {
      int yyn;
      if (yyarg)
        yyarg[yycount] = yyctx->yytoken;
      ++yycount;
      yyn = yypcontext_expected_tokens (yyctx,
                                        yyarg ? yyarg + 1 : yyarg, yyargn - 1);
      if (yyn == YYENOMEM)
        return YYENOMEM;
      else
        yycount += yyn;
    }
  return yycount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                const yypcontext_t *yyctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  yysymbol_kind_t yyarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* Actual size of YYARG. */
  int yycount = yy_syntax_error_arguments (yyctx, yyarg, YYARGS_MAX);
  if (yycount == YYENOMEM)
    return YYENOMEM;

  switch (yycount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        yyformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  yysize = yystrlen (yyformat) - 2 * yycount + 1;
  {
    int yyi;
    for (yyi = 0; yyi < yycount; ++yyi)
      {
        YYPTRDIFF_T yysize1
          = yysize + yytnamerr (YY_NULLPTR, yytname[yyarg[yyi]]);
        if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
          yysize = yysize1;
        else
          return YYENOMEM;
      }
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yytname[yyarg[yyi++]]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yykind)
    {
    case YYSYMBOL_NAME: /* NAME  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1871 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_NAME_PATTERN: /* NAME_PATTERN  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1877 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_STRING_VALUE: /* STRING_VALUE  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1883 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_INTEGER_VALUE: /* INTEGER_VALUE  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1889 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_DOUBLE_VALUE: /* DOUBLE_VALUE  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1895 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_UNIT: /* UNIT  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1901 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_identifier: /* identifier  */
#line 291 "../harp/libharp/harp-operation-parser.y"
            { free(((*yyvaluep).string_val)); }
#line 1907 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_double_array: /* double_array  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1913 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_int32_array: /* int32_array  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1919 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_string_array: /* string_array  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1925 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_identifier_array: /* identifier_array  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1931 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_identifier_pattern_array: /* identifier_pattern_array  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1937 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_dimension_array: /* dimension_array  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1943 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_dimensionspec: /* dimensionspec  */
#line 288 "../harp/libharp/harp-operation-parser.y"
            { harp_sized_array_delete(((*yyvaluep).array)); }
#line 1949 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_operation: /* operation  */
#line 289 "../harp/libharp/harp-operation-parser.y"
            { harp_operation_delete(((*yyvaluep).operation)); }
#line 1955 "libharp/harp-operation-parser.c"
        break;

    case YYSYMBOL_program: /* program  */
#line 290 "../harp/libharp/harp-operation-parser.y"
            { harp_program_delete(((*yyvaluep).program)); }
#line 1961 "libharp/harp-operation-parser.c"
        break;

      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* input: program ';'  */
#line 298 "../harp/libharp/harp-operation-parser.y"
                  { parsed_program = (yyvsp[-1].program); }
#line 2232 "libharp/harp-operation-parser.c"
    break;

  case 3: /* input: program  */
#line 299 "../harp/libharp/harp-operation-parser.y"
                  { parsed_program = (yyvsp[0].program); }
#line 2238 "libharp/harp-operation-parser.c"
    break;

  case 4: /* reserved_identifier: NAN  */
#line 303 "../harp/libharp/harp-operation-parser.y"
          { (yyval.const_string_val) = "nan"; }
#line 2244 "libharp/harp-operation-parser.c"
    break;

  case 5: /* reserved_identifier: INF  */
#line 304 "../harp/libharp/harp-operation-parser.y"
          { (yyval.const_string_val) = "inf"; }
#line 2250 "libharp/harp-operation-parser.c"
    break;

  case 6: /* reserved_identifier: NOT  */
#line 305 "../harp/libharp/harp-operation-parser.y"
          { (yyval.const_string_val) = "not"; }
#line 2256 "libharp/harp-operation-parser.c"
    break;

  case 7: /* reserved_identifier: IN  */
#line 306 "../harp/libharp/harp-operation-parser.y"
         { (yyval.const_string_val) = "in"; }
#line 2262 "libharp/harp-operation-parser.c"
    break;

  case 8: /* reserved_identifier: DIMENSION  */
#line 307 "../harp/libharp/harp-operation-parser.y"
                { (yyval.const_string_val) = harp_get_dimension_type_name((yyvsp[0].int32_val)); }
#line 2268 "libharp/harp-operation-parser.c"
    break;

  case 9: /* reserved_identifier: DATATYPE  */
#line 308 "../harp/libharp/harp-operation-parser.y"
               { (yyval.const_string_val) = harp_get_data_type_name((yyvsp[0].int32_val)); }
#line 2274 "libharp/harp-operation-parser.c"
    break;

  case 10: /* reserved_identifier: ID_A  */
#line 309 "../harp/libharp/harp-operation-parser.y"
           { (yyval.const_string_val) = "a"; }
#line 2280 "libharp/harp-operation-parser.c"
    break;

  case 11: /* reserved_identifier: ID_B  */
#line 310 "../harp/libharp/harp-operation-parser.y"
           { (yyval.const_string_val) = "b"; }
#line 2286 "libharp/harp-operation-parser.c"
    break;

  case 12: /* reserved_identifier: FUNC_AREA_COVERS_AREA  */
#line 311 "../harp/libharp/harp-operation-parser.y"
                            { (yyval.const_string_val) = "area_covers_area"; }
#line 2292 "libharp/harp-operation-parser.c"
    break;

  case 13: /* reserved_identifier: FUNC_AREA_COVERS_POINT  */
#line 312 "../harp/libharp/harp-operation-parser.y"
                             { (yyval.const_string_val) = "area_covers_point"; }
#line 2298 "libharp/harp-operation-parser.c"
    break;

  case 14: /* reserved_identifier: FUNC_AREA_INSIDE_AREA  */
#line 313 "../harp/libharp/harp-operation-parser.y"
                            { (yyval.const_string_val) = "area_inside_area"; }
#line 2304 "libharp/harp-operation-parser.c"
    break;

  case 15: /* reserved_identifier: FUNC_AREA_INTERSECTS_AREA  */
#line 314 "../harp/libharp/harp-operation-parser.y"
                                { (yyval.const_string_val) = "area_intersects_area"; }
#line 2310 "libharp/harp-operation-parser.c"
    break;

  case 16: /* reserved_identifier: FUNC_BIN  */
#line 315 "../harp/libharp/harp-operation-parser.y"
               { (yyval.const_string_val) = "bin"; }
#line 2316 "libharp/harp-operation-parser.c"
    break;

  case 17: /* reserved_identifier: FUNC_BIN_SPATIAL  */
#line 316 "../harp/libharp/harp-operation-parser.y"
                       { (yyval.const_string_val) = "bin_spatial"; }
#line 2322 "libharp/harp-operation-parser.c"
    break;

  case 18: /* reserved_identifier: FUNC_CLAMP  */
#line 317 "../harp/libharp/harp-operation-parser.y"
                 { (yyval.const_string_val) = "clamp"; }
#line 2328 "libharp/harp-operation-parser.c"
    break;

  case 19: /* reserved_identifier: FUNC_COLLOCATE_LEFT  */
#line 318 "../harp/libharp/harp-operation-parser.y"
                          { (yyval.const_string_val) = "collocate_left"; }
#line 2334 "libharp/harp-operation-parser.c"
    break;

  case 20: /* reserved_identifier: FUNC_COLLOCATE_RIGHT  */
#line 319 "../harp/libharp/harp-operation-parser.y"
                           { (yyval.const_string_val) = "collocate_right"; }
#line 2340 "libharp/harp-operation-parser.c"
    break;

  case 21: /* reserved_identifier: FUNC_DERIVE  */
#line 320 "../harp/libharp/harp-operation-parser.y"
                  { (yyval.const_string_val) = "derive"; }
#line 2346 "libharp/harp-operation-parser.c"
    break;

  case 22: /* reserved_identifier: FUNC_DERIVE_SMOOTHED_COLUMN  */
#line 321 "../harp/libharp/harp-operation-parser.y"
                                  { (yyval.const_string_val) = "derive_smoothed_column"; }
#line 2352 "libharp/harp-operation-parser.c"
    break;

  case 23: /* reserved_identifier: FUNC_EXCLUDE  */
#line 322 "../harp/libharp/harp-operation-parser.y"
                   { (yyval.const_string_val) = "exclude"; }
#line 2358 "libharp/harp-operation-parser.c"
    break;

  case 24: /* reserved_identifier: FUNC_FLATTEN  */
#line 323 "../harp/libharp/harp-operation-parser.y"
                   { (yyval.const_string_val) = "flatten"; }
#line 2364 "libharp/harp-operation-parser.c"
    break;

  case 25: /* reserved_identifier: FUNC_INDEX  */
#line 324 "../harp/libharp/harp-operation-parser.y"
                 { (yyval.const_string_val) = "index"; }
#line 2370 "libharp/harp-operation-parser.c"
    break;

  case 26: /* reserved_identifier: FUNC_KEEP  */
#line 325 "../harp/libharp/harp-operation-parser.y"
                { (yyval.const_string_val) = "keep"; }
#line 2376 "libharp/harp-operation-parser.c"
    break;

  case 27: /* reserved_identifier: FUNC_LONGITUDE_RANGE  */
#line 326 "../harp/libharp/harp-operation-parser.y"
                           { (yyval.const_string_val) = "longitude_range"; }
#line 2382 "libharp/harp-operation-parser.c"
    break;

  case 28: /* reserved_identifier: FUNC_POINT_DISTANCE  */
#line 327 "../harp/libharp/harp-operation-parser.y"
                          { (yyval.const_string_val) = "point_distance"; }
#line 2388 "libharp/harp-operation-parser.c"
    break;

  case 29: /* reserved_identifier: FUNC_POINT_IN_AREA  */
#line 328 "../harp/libharp/harp-operation-parser.y"
                         { (yyval.const_string_val) = "point_in_area"; }
#line 2394 "libharp/harp-operation-parser.c"
    break;

  case 30: /* reserved_identifier: FUNC_REBIN  */
#line 329 "../harp/libharp/harp-operation-parser.y"
                 { (yyval.const_string_val) = "rebin"; }
#line 2400 "libharp/harp-operation-parser.c"
    break;

  case 31: /* reserved_identifier: FUNC_REGRID  */
#line 330 "../harp/libharp/harp-operation-parser.y"
                  { (yyval.const_string_val) = "regrid"; }
#line 2406 "libharp/harp-operation-parser.c"
    break;

  case 32: /* reserved_identifier: FUNC_RENAME  */
#line 331 "../harp/libharp/harp-operation-parser.y"
                  { (yyval.const_string_val) = "rename"; }
#line 2412 "libharp/harp-operation-parser.c"
    break;

  case 33: /* reserved_identifier: FUNC_SET  */
#line 332 "../harp/libharp/harp-operation-parser.y"
               { (yyval.const_string_val) = "set"; }
#line 2418 "libharp/harp-operation-parser.c"
    break;

  case 34: /* reserved_identifier: FUNC_SMOOTH  */
#line 333 "../harp/libharp/harp-operation-parser.y"
                  { (yyval.const_string_val) = "smooth"; }
#line 2424 "libharp/harp-operation-parser.c"
    break;

  case 35: /* reserved_identifier: FUNC_SORT  */
#line 334 "../harp/libharp/harp-operation-parser.y"
                { (yyval.const_string_val) = "sort"; }
#line 2430 "libharp/harp-operation-parser.c"
    break;

  case 36: /* reserved_identifier: FUNC_SQUASH  */
#line 335 "../harp/libharp/harp-operation-parser.y"
                  { (yyval.const_string_val) = "squash"; }
#line 2436 "libharp/harp-operation-parser.c"
    break;

  case 37: /* reserved_identifier: FUNC_VALID  */
#line 336 "../harp/libharp/harp-operation-parser.y"
                 { (yyval.const_string_val) = "valid"; }
#line 2442 "libharp/harp-operation-parser.c"
    break;

  case 38: /* reserved_identifier: FUNC_WRAP  */
#line 337 "../harp/libharp/harp-operation-parser.y"
                { (yyval.const_string_val) = "wrap"; }
#line 2448 "libharp/harp-operation-parser.c"
    break;

  case 40: /* identifier: reserved_identifier  */
#line 342 "../harp/libharp/harp-operation-parser.y"
                          { (yyval.string_val) = strdup((yyvsp[0].const_string_val)); }
#line 2454 "libharp/harp-operation-parser.c"
    break;

  case 43: /* double_value: DOUBLE_VALUE  */
#line 351 "../harp/libharp/harp-operation-parser.y"
                   {
            long length = (long)strlen((yyvsp[0].string_val));
            if (harp_parse_double((yyvsp[0].string_val), length, &(yyval.double_val), 0) != length)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2468 "libharp/harp-operation-parser.c"
    break;

  case 44: /* double_value: '+' DOUBLE_VALUE  */
#line 360 "../harp/libharp/harp-operation-parser.y"
                       {
            long length = (long)strlen((yyvsp[0].string_val));
            if (harp_parse_double((yyvsp[0].string_val), length, &(yyval.double_val), 0) != length)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2482 "libharp/harp-operation-parser.c"
    break;

  case 45: /* double_value: '-' DOUBLE_VALUE  */
#line 369 "../harp/libharp/harp-operation-parser.y"
                       {
            long length = (long)strlen((yyvsp[0].string_val));
            if (harp_parse_double((yyvsp[0].string_val), length, &(yyval.double_val), 0) != length)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
            (yyval.double_val) = -(yyval.double_val);
        }
#line 2497 "libharp/harp-operation-parser.c"
    break;

  case 46: /* double_value: int32_value  */
#line 379 "../harp/libharp/harp-operation-parser.y"
                  { (yyval.double_val) = (double)(yyvsp[0].int32_val); }
#line 2503 "libharp/harp-operation-parser.c"
    break;

  case 47: /* double_value: NAN  */
#line 380 "../harp/libharp/harp-operation-parser.y"
          { (yyval.double_val) = harp_nan(); }
#line 2509 "libharp/harp-operation-parser.c"
    break;

  case 48: /* double_value: '+' NAN  */
#line 381 "../harp/libharp/harp-operation-parser.y"
              { (yyval.double_val) = harp_nan(); }
#line 2515 "libharp/harp-operation-parser.c"
    break;

  case 49: /* double_value: '-' NAN  */
#line 382 "../harp/libharp/harp-operation-parser.y"
              { (yyval.double_val) = harp_nan(); }
#line 2521 "libharp/harp-operation-parser.c"
    break;

  case 50: /* double_value: '+' INF  */
#line 383 "../harp/libharp/harp-operation-parser.y"
              { (yyval.double_val) = harp_plusinf(); }
#line 2527 "libharp/harp-operation-parser.c"
    break;

  case 51: /* double_value: '-' INF  */
#line 384 "../harp/libharp/harp-operation-parser.y"
              { (yyval.double_val) = harp_mininf(); }
#line 2533 "libharp/harp-operation-parser.c"
    break;

  case 52: /* int32_value: INTEGER_VALUE  */
#line 388 "../harp/libharp/harp-operation-parser.y"
                    {
            if (parse_int32((yyvsp[0].string_val), &(yyval.int32_val)) != 0)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2546 "libharp/harp-operation-parser.c"
    break;

  case 53: /* int32_value: '+' INTEGER_VALUE  */
#line 396 "../harp/libharp/harp-operation-parser.y"
                        {
            if (parse_int32((yyvsp[0].string_val), &(yyval.int32_val)) != 0)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2559 "libharp/harp-operation-parser.c"
    break;

  case 54: /* int32_value: '-' INTEGER_VALUE  */
#line 404 "../harp/libharp/harp-operation-parser.y"
                        {
            if (parse_int32((yyvsp[0].string_val), &(yyval.int32_val)) != 0)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            (yyval.int32_val) = -(yyval.int32_val);
            free((yyvsp[0].string_val));
        }
#line 2573 "libharp/harp-operation-parser.c"
    break;

  case 55: /* double_array: double_array ',' double_value  */
#line 416 "../harp/libharp/harp-operation-parser.y"
                                    {
            if (harp_sized_array_add_double((yyvsp[-2].array), (yyvsp[0].double_val)) != 0) YYERROR;
            (yyval.array) = (yyvsp[-2].array);
        }
#line 2582 "libharp/harp-operation-parser.c"
    break;

  case 56: /* double_array: double_value  */
#line 420 "../harp/libharp/harp-operation-parser.y"
                   {
            if (harp_sized_array_new(harp_type_double, &(yyval.array)) != 0) YYERROR;
            if (harp_sized_array_add_double((yyval.array), (yyvsp[0].double_val)) != 0) YYERROR;
        }
#line 2591 "libharp/harp-operation-parser.c"
    break;

  case 57: /* int32_array: int32_array ',' int32_value  */
#line 427 "../harp/libharp/harp-operation-parser.y"
                                  {
            if (harp_sized_array_add_int32((yyvsp[-2].array), (yyvsp[0].int32_val)) != 0) YYERROR;
            (yyval.array) = (yyvsp[-2].array);
        }
#line 2600 "libharp/harp-operation-parser.c"
    break;

  case 58: /* int32_array: int32_value  */
#line 431 "../harp/libharp/harp-operation-parser.y"
                  {
            if (harp_sized_array_new(harp_type_int32, &(yyval.array)) != 0) YYERROR;
            if (harp_sized_array_add_int32((yyval.array), (yyvsp[0].int32_val)) != 0) YYERROR;
        }
#line 2609 "libharp/harp-operation-parser.c"
    break;

  case 59: /* string_array: string_array ',' STRING_VALUE  */
#line 438 "../harp/libharp/harp-operation-parser.y"
                                    {
            if (harp_sized_array_add_string((yyvsp[-2].array), (yyvsp[0].string_val)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            (yyval.array) = (yyvsp[-2].array);
            free((yyvsp[0].string_val));
        }
#line 2624 "libharp/harp-operation-parser.c"
    break;

  case 60: /* string_array: STRING_VALUE  */
#line 448 "../harp/libharp/harp-operation-parser.y"
                   {
            if (harp_sized_array_new(harp_type_string, &(yyval.array)) != 0)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            if (harp_sized_array_add_string((yyval.array), (yyvsp[0].string_val)) != 0)
            {
                harp_sized_array_delete((yyval.array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2643 "libharp/harp-operation-parser.c"
    break;

  case 61: /* identifier_array: identifier_array ',' identifier  */
#line 465 "../harp/libharp/harp-operation-parser.y"
                                      {
            if (harp_sized_array_add_string((yyvsp[-2].array), (yyvsp[0].string_val)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            (yyval.array) = (yyvsp[-2].array);
            free((yyvsp[0].string_val));
        }
#line 2658 "libharp/harp-operation-parser.c"
    break;

  case 62: /* identifier_array: identifier  */
#line 475 "../harp/libharp/harp-operation-parser.y"
                 {
            if (harp_sized_array_new(harp_type_string, &(yyval.array)) != 0)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            if (harp_sized_array_add_string((yyval.array), (yyvsp[0].string_val)) != 0)
            {
                harp_sized_array_delete((yyval.array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2677 "libharp/harp-operation-parser.c"
    break;

  case 63: /* identifier_pattern_array: identifier_pattern_array ',' identifier_pattern  */
#line 492 "../harp/libharp/harp-operation-parser.y"
                                                      {
            if (harp_sized_array_add_string((yyvsp[-2].array), (yyvsp[0].string_val)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            (yyval.array) = (yyvsp[-2].array);
            free((yyvsp[0].string_val));
        }
#line 2692 "libharp/harp-operation-parser.c"
    break;

  case 64: /* identifier_pattern_array: identifier_pattern  */
#line 502 "../harp/libharp/harp-operation-parser.y"
                         {
            if (harp_sized_array_new(harp_type_string, &(yyval.array)) != 0)
            {
                free((yyvsp[0].string_val));
                YYERROR;
            }
            if (harp_sized_array_add_string((yyval.array), (yyvsp[0].string_val)) != 0)
            {
                harp_sized_array_delete((yyval.array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[0].string_val));
        }
#line 2711 "libharp/harp-operation-parser.c"
    break;

  case 65: /* dimension_array: dimension_array ',' DIMENSION  */
#line 519 "../harp/libharp/harp-operation-parser.y"
                                    {
            if (harp_sized_array_add_int32((yyvsp[-2].array), (yyvsp[0].int32_val)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            (yyval.array) = (yyvsp[-2].array);
        }
#line 2724 "libharp/harp-operation-parser.c"
    break;

  case 66: /* dimension_array: DIMENSION  */
#line 527 "../harp/libharp/harp-operation-parser.y"
                {
            if (harp_sized_array_new(harp_type_int32, &(yyval.array)) != 0) YYERROR;
            if (harp_sized_array_add_int32((yyval.array), (yyvsp[0].int32_val)) != 0)
            {
                harp_sized_array_delete((yyval.array));
                YYERROR;
            }
        }
#line 2737 "libharp/harp-operation-parser.c"
    break;

  case 67: /* dimensionspec: '{' dimension_array '}'  */
#line 538 "../harp/libharp/harp-operation-parser.y"
                              { (yyval.array) = (yyvsp[-1].array); }
#line 2743 "libharp/harp-operation-parser.c"
    break;

  case 68: /* dimensionspec: '{' '}'  */
#line 539 "../harp/libharp/harp-operation-parser.y"
              { if (harp_sized_array_new(harp_type_int32, &(yyval.array)) != 0) YYERROR; }
#line 2749 "libharp/harp-operation-parser.c"
    break;

  case 69: /* comparison_operator: EQUAL  */
#line 543 "../harp/libharp/harp-operation-parser.y"
            { (yyval.comparison_operator) = operator_eq; }
#line 2755 "libharp/harp-operation-parser.c"
    break;

  case 70: /* comparison_operator: NOT_EQUAL  */
#line 544 "../harp/libharp/harp-operation-parser.y"
                { (yyval.comparison_operator) = operator_ne; }
#line 2761 "libharp/harp-operation-parser.c"
    break;

  case 71: /* comparison_operator: GREATER_EQUAL  */
#line 545 "../harp/libharp/harp-operation-parser.y"
                    { (yyval.comparison_operator) = operator_ge; }
#line 2767 "libharp/harp-operation-parser.c"
    break;

  case 72: /* comparison_operator: LESS_EQUAL  */
#line 546 "../harp/libharp/harp-operation-parser.y"
                 { (yyval.comparison_operator) = operator_le; }
#line 2773 "libharp/harp-operation-parser.c"
    break;

  case 73: /* comparison_operator: '>'  */
#line 547 "../harp/libharp/harp-operation-parser.y"
          { (yyval.comparison_operator) = operator_gt; }
#line 2779 "libharp/harp-operation-parser.c"
    break;

  case 74: /* comparison_operator: '<'  */
#line 548 "../harp/libharp/harp-operation-parser.y"
          { (yyval.comparison_operator) = operator_lt; }
#line 2785 "libharp/harp-operation-parser.c"
    break;

  case 75: /* bit_mask_operator: BIT_NAND  */
#line 552 "../harp/libharp/harp-operation-parser.y"
               { (yyval.bit_mask_operator) = operator_bit_mask_none; }
#line 2791 "libharp/harp-operation-parser.c"
    break;

  case 76: /* bit_mask_operator: BIT_AND  */
#line 553 "../harp/libharp/harp-operation-parser.y"
              { (yyval.bit_mask_operator) = operator_bit_mask_any; }
#line 2797 "libharp/harp-operation-parser.c"
    break;

  case 77: /* membership_operator: NOT IN  */
#line 557 "../harp/libharp/harp-operation-parser.y"
             { (yyval.membership_operator) = operator_not_in; }
#line 2803 "libharp/harp-operation-parser.c"
    break;

  case 78: /* membership_operator: IN  */
#line 558 "../harp/libharp/harp-operation-parser.y"
         { (yyval.membership_operator) = operator_in; }
#line 2809 "libharp/harp-operation-parser.c"
    break;

  case 79: /* operation: identifier bit_mask_operator int32_value  */
#line 562 "../harp/libharp/harp-operation-parser.y"
                                               {
            if (harp_operation_bit_mask_filter_new((yyvsp[-2].string_val), (yyvsp[-1].bit_mask_operator), (yyvsp[0].int32_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-2].string_val));
                YYERROR;
            }
            free((yyvsp[-2].string_val));
        }
#line 2822 "libharp/harp-operation-parser.c"
    break;

  case 80: /* operation: identifier comparison_operator double_value  */
#line 570 "../harp/libharp/harp-operation-parser.y"
                                                  {
            if (harp_operation_comparison_filter_new((yyvsp[-2].string_val), (yyvsp[-1].comparison_operator), (yyvsp[0].double_val), NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-2].string_val));
                YYERROR;
            }
            free((yyvsp[-2].string_val));
        }
#line 2835 "libharp/harp-operation-parser.c"
    break;

  case 81: /* operation: identifier comparison_operator double_value UNIT  */
#line 578 "../harp/libharp/harp-operation-parser.y"
                                                       {
            if (harp_operation_comparison_filter_new((yyvsp[-3].string_val), (yyvsp[-2].comparison_operator), (yyvsp[-1].double_val), (yyvsp[0].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
            free((yyvsp[0].string_val));
        }
#line 2850 "libharp/harp-operation-parser.c"
    break;

  case 82: /* operation: identifier comparison_operator STRING_VALUE  */
#line 588 "../harp/libharp/harp-operation-parser.y"
                                                  {
            if (harp_operation_string_comparison_filter_new((yyvsp[-2].string_val), (yyvsp[-1].comparison_operator), (yyvsp[0].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-2].string_val));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[-2].string_val));
            free((yyvsp[0].string_val));
        }
#line 2865 "libharp/harp-operation-parser.c"
    break;

  case 83: /* operation: identifier membership_operator '(' double_array ')' UNIT  */
#line 598 "../harp/libharp/harp-operation-parser.y"
                                                               {
            if (harp_operation_membership_filter_new((yyvsp[-5].string_val), (yyvsp[-4].membership_operator), (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, (yyvsp[0].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                free((yyvsp[0].string_val));
                YYERROR;
            }
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
            free((yyvsp[0].string_val));
        }
#line 2882 "libharp/harp-operation-parser.c"
    break;

  case 84: /* operation: identifier membership_operator '(' double_array ')'  */
#line 610 "../harp/libharp/harp-operation-parser.y"
                                                          {
            if (harp_operation_membership_filter_new((yyvsp[-4].string_val), (yyvsp[-3].membership_operator), (yyvsp[-1].array)->num_elements, (yyvsp[-1].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-4].string_val));
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 2897 "libharp/harp-operation-parser.c"
    break;

  case 85: /* operation: identifier membership_operator '(' string_array ')'  */
#line 620 "../harp/libharp/harp-operation-parser.y"
                                                          {
            if (harp_operation_string_membership_filter_new((yyvsp[-4].string_val), (yyvsp[-3].membership_operator), (yyvsp[-1].array)->num_elements,
                                                            (const char **)(yyvsp[-1].array)->array.string_data, &(yyval.operation)) != 0)
            {
                free((yyvsp[-4].string_val));
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 2913 "libharp/harp-operation-parser.c"
    break;

  case 86: /* operation: FUNC_INDEX '(' DIMENSION ')' comparison_operator int32_value  */
#line 631 "../harp/libharp/harp-operation-parser.y"
                                                                   {
            if (harp_operation_index_comparison_filter_new((yyvsp[-3].int32_val), (yyvsp[-1].comparison_operator), (yyvsp[0].int32_val), &(yyval.operation)) != 0)
            {
                YYERROR;
            }
        }
#line 2924 "libharp/harp-operation-parser.c"
    break;

  case 87: /* operation: FUNC_INDEX '(' DIMENSION ')' membership_operator '(' int32_array ')'  */
#line 637 "../harp/libharp/harp-operation-parser.y"
                                                                           {
            if (harp_operation_index_membership_filter_new((yyvsp[-5].int32_val), (yyvsp[-3].membership_operator), (yyvsp[-1].array)->num_elements, (yyvsp[-1].array)->array.int32_data, &(yyval.operation)) !=
                0)
            {
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 2938 "libharp/harp-operation-parser.c"
    break;

  case 88: /* operation: FUNC_AREA_COVERS_AREA '(' '(' double_array ')' ',' '(' double_array ')' ')'  */
#line 646 "../harp/libharp/harp-operation-parser.y"
                                                                                  {
            if (harp_operation_area_covers_area_filter_new(NULL, (yyvsp[-6].array)->num_elements, (yyvsp[-6].array)->array.double_data, NULL,
                                                           (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-6].array));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-6].array));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 2954 "libharp/harp-operation-parser.c"
    break;

  case 89: /* operation: FUNC_AREA_COVERS_AREA '(' '(' double_array ')' ',' '(' double_array ')' UNIT ')'  */
#line 657 "../harp/libharp/harp-operation-parser.y"
                                                                                       {
            if (harp_operation_area_covers_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, NULL,
                                                           (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 2972 "libharp/harp-operation-parser.c"
    break;

  case 90: /* operation: FUNC_AREA_COVERS_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' ')'  */
#line 670 "../harp/libharp/harp-operation-parser.y"
                                                                                       {
            if (harp_operation_area_covers_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, (yyvsp[-5].string_val),
                                                           (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 2990 "libharp/harp-operation-parser.c"
    break;

  case 91: /* operation: FUNC_AREA_COVERS_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' UNIT ')'  */
#line 683 "../harp/libharp/harp-operation-parser.y"
                                                                                            {
            if (harp_operation_area_covers_area_filter_new(NULL, (yyvsp[-8].array)->num_elements, (yyvsp[-8].array)->array.double_data, (yyvsp[-6].string_val),
                                                           (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-8].array));
                free((yyvsp[-6].string_val));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-8].array));
            free((yyvsp[-6].string_val));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 3010 "libharp/harp-operation-parser.c"
    break;

  case 92: /* operation: FUNC_AREA_COVERS_AREA '(' STRING_VALUE ')'  */
#line 698 "../harp/libharp/harp-operation-parser.y"
                                                 {
            if (harp_operation_area_covers_area_filter_new((yyvsp[-1].string_val), 0, NULL, NULL, 0, NULL, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3023 "libharp/harp-operation-parser.c"
    break;

  case 93: /* operation: FUNC_AREA_COVERS_POINT '(' double_value ',' double_value ')'  */
#line 706 "../harp/libharp/harp-operation-parser.y"
                                                                   {
            if (harp_operation_area_covers_point_filter_new((yyvsp[-3].double_val), NULL, (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0) YYERROR;
        }
#line 3031 "libharp/harp-operation-parser.c"
    break;

  case 94: /* operation: FUNC_AREA_COVERS_POINT '(' double_value ',' double_value UNIT ')'  */
#line 709 "../harp/libharp/harp-operation-parser.y"
                                                                        {
            if (harp_operation_area_covers_point_filter_new((yyvsp[-4].double_val), NULL, (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3044 "libharp/harp-operation-parser.c"
    break;

  case 95: /* operation: FUNC_AREA_COVERS_POINT '(' double_value UNIT ',' double_value ')'  */
#line 717 "../harp/libharp/harp-operation-parser.y"
                                                                        {
            if (harp_operation_area_covers_point_filter_new((yyvsp[-4].double_val), (yyvsp[-3].string_val), (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3057 "libharp/harp-operation-parser.c"
    break;

  case 96: /* operation: FUNC_AREA_COVERS_POINT '(' double_value UNIT ',' double_value UNIT ')'  */
#line 725 "../harp/libharp/harp-operation-parser.y"
                                                                             {
            if (harp_operation_area_covers_point_filter_new((yyvsp[-5].double_val), (yyvsp[-4].string_val), (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-4].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3072 "libharp/harp-operation-parser.c"
    break;

  case 97: /* operation: FUNC_AREA_INSIDE_AREA '(' '(' double_array ')' ',' '(' double_array ')' ')'  */
#line 735 "../harp/libharp/harp-operation-parser.y"
                                                                                  {
            if (harp_operation_area_inside_area_filter_new(NULL, (yyvsp[-6].array)->num_elements, (yyvsp[-6].array)->array.double_data, NULL,
                                                           (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-6].array));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-6].array));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3088 "libharp/harp-operation-parser.c"
    break;

  case 98: /* operation: FUNC_AREA_INSIDE_AREA '(' '(' double_array ')' ',' '(' double_array ')' UNIT ')'  */
#line 746 "../harp/libharp/harp-operation-parser.y"
                                                                                       {
            if (harp_operation_area_inside_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, NULL,
                                                           (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 3106 "libharp/harp-operation-parser.c"
    break;

  case 99: /* operation: FUNC_AREA_INSIDE_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' ')'  */
#line 759 "../harp/libharp/harp-operation-parser.y"
                                                                                       {
            if (harp_operation_area_inside_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, (yyvsp[-5].string_val),
                                                           (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3124 "libharp/harp-operation-parser.c"
    break;

  case 100: /* operation: FUNC_AREA_INSIDE_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' UNIT ')'  */
#line 772 "../harp/libharp/harp-operation-parser.y"
                                                                                            {
            if (harp_operation_area_inside_area_filter_new(NULL, (yyvsp[-8].array)->num_elements, (yyvsp[-8].array)->array.double_data, (yyvsp[-6].string_val),
                                                           (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-8].array));
                free((yyvsp[-6].string_val));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-8].array));
            free((yyvsp[-6].string_val));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 3144 "libharp/harp-operation-parser.c"
    break;

  case 101: /* operation: FUNC_AREA_INSIDE_AREA '(' STRING_VALUE ')'  */
#line 787 "../harp/libharp/harp-operation-parser.y"
                                                 {
            if (harp_operation_area_inside_area_filter_new((yyvsp[-1].string_val), 0, NULL, NULL, 0, NULL, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3157 "libharp/harp-operation-parser.c"
    break;

  case 102: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' ',' '(' double_array ')' ')'  */
#line 795 "../harp/libharp/harp-operation-parser.y"
                                                                                      {
            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-6].array)->num_elements, (yyvsp[-6].array)->array.double_data, NULL,
                                                               (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, NULL, &(yyval.operation))
                != 0)
            {
                harp_sized_array_delete((yyvsp[-6].array));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-6].array));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3174 "libharp/harp-operation-parser.c"
    break;

  case 103: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' ',' '(' double_array ')' UNIT ')'  */
#line 807 "../harp/libharp/harp-operation-parser.y"
                                                                                           {
            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, NULL,
                                                               (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), NULL, &(yyval.operation))
                != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 3193 "libharp/harp-operation-parser.c"
    break;

  case 104: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' ')'  */
#line 821 "../harp/libharp/harp-operation-parser.y"
                                                                                           {
            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, (yyvsp[-5].string_val),
                                                               (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, NULL, &(yyval.operation))
                != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3212 "libharp/harp-operation-parser.c"
    break;

  case 105: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' UNIT ')'  */
#line 835 "../harp/libharp/harp-operation-parser.y"
                                                                                                {
            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-8].array)->num_elements, (yyvsp[-8].array)->array.double_data, (yyvsp[-6].string_val),
                                                               (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), NULL, &(yyval.operation))
                != 0)
            {
                harp_sized_array_delete((yyvsp[-8].array));
                free((yyvsp[-6].string_val));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-8].array));
            free((yyvsp[-6].string_val));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 3233 "libharp/harp-operation-parser.c"
    break;

  case 106: /* operation: FUNC_AREA_INTERSECTS_AREA '(' STRING_VALUE ')'  */
#line 851 "../harp/libharp/harp-operation-parser.y"
                                                     {
            if (harp_operation_area_intersects_area_filter_new((yyvsp[-1].string_val), 0, NULL, NULL, 0, NULL, NULL, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3246 "libharp/harp-operation-parser.c"
    break;

  case 107: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' ',' '(' double_array ')' ',' double_value ')'  */
#line 859 "../harp/libharp/harp-operation-parser.y"
                                                                                                       {
            double min_fraction = (yyvsp[-1].double_val);

            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-8].array)->num_elements, (yyvsp[-8].array)->array.double_data, NULL,
                                                               (yyvsp[-4].array)->num_elements, (yyvsp[-4].array)->array.double_data, NULL,
                                                               &min_fraction, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-8].array));
                harp_sized_array_delete((yyvsp[-4].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-8].array));
            harp_sized_array_delete((yyvsp[-4].array));
        }
#line 3265 "libharp/harp-operation-parser.c"
    break;

  case 108: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' ',' '(' double_array ')' UNIT ',' double_value ')'  */
#line 873 "../harp/libharp/harp-operation-parser.y"
                                                                                                            {
            double min_fraction = (yyvsp[-1].double_val);

            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-9].array)->num_elements, (yyvsp[-9].array)->array.double_data, NULL,
                                                               (yyvsp[-5].array)->num_elements, (yyvsp[-5].array)->array.double_data, (yyvsp[-3].string_val),
                                                               &min_fraction, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-9].array));
                harp_sized_array_delete((yyvsp[-5].array));
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-9].array));
            harp_sized_array_delete((yyvsp[-5].array));
            free((yyvsp[-3].string_val));
        }
#line 3286 "libharp/harp-operation-parser.c"
    break;

  case 109: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' ',' double_value ')'  */
#line 889 "../harp/libharp/harp-operation-parser.y"
                                                                                                            {
            double min_fraction = (yyvsp[-1].double_val);

            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-9].array)->num_elements, (yyvsp[-9].array)->array.double_data, (yyvsp[-7].string_val),
                                                               (yyvsp[-4].array)->num_elements, (yyvsp[-4].array)->array.double_data, NULL,
                                                               &min_fraction, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-9].array));
                free((yyvsp[-7].string_val));
                harp_sized_array_delete((yyvsp[-4].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-9].array));
            free((yyvsp[-7].string_val));
            harp_sized_array_delete((yyvsp[-4].array));
        }
#line 3307 "libharp/harp-operation-parser.c"
    break;

  case 110: /* operation: FUNC_AREA_INTERSECTS_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' UNIT ',' double_value ')'  */
#line 905 "../harp/libharp/harp-operation-parser.y"
                                                                                                                 {
            double min_fraction = (yyvsp[-1].double_val);

            if (harp_operation_area_intersects_area_filter_new(NULL, (yyvsp[-10].array)->num_elements, (yyvsp[-10].array)->array.double_data, (yyvsp[-8].string_val),
                                                               (yyvsp[-5].array)->num_elements, (yyvsp[-5].array)->array.double_data, (yyvsp[-3].string_val),
                                                               &min_fraction, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-10].array));
                free((yyvsp[-8].string_val));
                harp_sized_array_delete((yyvsp[-5].array));
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-10].array));
            free((yyvsp[-8].string_val));
            harp_sized_array_delete((yyvsp[-5].array));
            free((yyvsp[-3].string_val));
        }
#line 3330 "libharp/harp-operation-parser.c"
    break;

  case 111: /* operation: FUNC_AREA_INTERSECTS_AREA '(' STRING_VALUE ',' double_value ')'  */
#line 923 "../harp/libharp/harp-operation-parser.y"
                                                                      {
            double min_fraction = (yyvsp[-1].double_val);

            if (harp_operation_area_intersects_area_filter_new((yyvsp[-3].string_val), 0, NULL, NULL, 0, NULL, NULL, &min_fraction, &(yyval.operation)) !=
                0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3346 "libharp/harp-operation-parser.c"
    break;

  case 112: /* operation: FUNC_BIN '(' ')'  */
#line 934 "../harp/libharp/harp-operation-parser.y"
                       {
            if (harp_operation_bin_full_new(&(yyval.operation)) != 0)
            {
                YYERROR;
            }
        }
#line 3357 "libharp/harp-operation-parser.c"
    break;

  case 113: /* operation: FUNC_BIN '(' identifier ')'  */
#line 940 "../harp/libharp/harp-operation-parser.y"
                                  {
            if (harp_operation_bin_with_variables_new(1, (const char **)&(yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3370 "libharp/harp-operation-parser.c"
    break;

  case 114: /* operation: FUNC_BIN '(' '(' identifier_array ')' ')'  */
#line 948 "../harp/libharp/harp-operation-parser.y"
                                                {
            if (harp_operation_bin_with_variables_new((yyvsp[-2].array)->num_elements, (const char **)(yyvsp[-2].array)->array.string_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3383 "libharp/harp-operation-parser.c"
    break;

  case 115: /* operation: FUNC_BIN '(' STRING_VALUE ',' ID_A ')'  */
#line 956 "../harp/libharp/harp-operation-parser.y"
                                             {
            if (harp_operation_bin_collocated_new((yyvsp[-3].string_val), 'a', &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3396 "libharp/harp-operation-parser.c"
    break;

  case 116: /* operation: FUNC_BIN '(' STRING_VALUE ',' ID_B ')'  */
#line 964 "../harp/libharp/harp-operation-parser.y"
                                             {
            if (harp_operation_bin_collocated_new((yyvsp[-3].string_val), 'b', &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3409 "libharp/harp-operation-parser.c"
    break;

  case 117: /* operation: FUNC_BIN_SPATIAL '(' '(' double_array ')' ',' '(' double_array ')' ')'  */
#line 972 "../harp/libharp/harp-operation-parser.y"
                                                                             {
            if (harp_operation_bin_spatial_new((yyvsp[-6].array)->num_elements, (yyvsp[-6].array)->array.double_data,
                                               (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-6].array));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-6].array));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3425 "libharp/harp-operation-parser.c"
    break;

  case 118: /* operation: FUNC_BIN_SPATIAL '(' int32_value ',' double_value ',' double_value ',' int32_value ',' double_value ',' double_value ')'  */
#line 984 "../harp/libharp/harp-operation-parser.y"
                       {
            harp_sized_array *lat_array;
            harp_sized_array *lon_array;
            long i;

            if (harp_sized_array_new(harp_type_double, &lat_array) != 0)
            {
                YYERROR;
            }
            for (i = 0; i < (yyvsp[-11].int32_val); i++)
            {
                if (harp_sized_array_add_double(lat_array, (yyvsp[-9].double_val) + i * (yyvsp[-7].double_val)) != 0)
                {
                    harp_sized_array_delete(lat_array);
                    YYERROR;
                }
            }
            if (harp_sized_array_new(harp_type_double, &lon_array) != 0)
            {
                harp_sized_array_delete(lat_array);
                YYERROR;
            }
            for (i = 0; i < (yyvsp[-5].int32_val); i++)
            {
                if (harp_sized_array_add_double(lon_array, (yyvsp[-3].double_val) + i * (yyvsp[-1].double_val)) != 0)
                {
                    harp_sized_array_delete(lat_array);
                    harp_sized_array_delete(lon_array);
                    YYERROR;
                }
            }
            if (harp_operation_bin_spatial_new(lat_array->num_elements, lat_array->array.double_data,
                                               lon_array->num_elements, lon_array->array.double_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete(lat_array);
                harp_sized_array_delete(lon_array);
                YYERROR;
            }
            harp_sized_array_delete(lat_array);
            harp_sized_array_delete(lon_array);
        }
#line 3471 "libharp/harp-operation-parser.c"
    break;

  case 119: /* operation: FUNC_CLAMP '(' DIMENSION ',' identifier UNIT ',' '(' double_value ',' double_value ')' ')'  */
#line 1025 "../harp/libharp/harp-operation-parser.y"
                                                                                                 {
            if (harp_operation_clamp_new((yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-4].double_val), (yyvsp[-2].double_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                YYERROR;
            }
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
        }
#line 3486 "libharp/harp-operation-parser.c"
    break;

  case 120: /* operation: FUNC_COLLOCATE_LEFT '(' STRING_VALUE ')'  */
#line 1035 "../harp/libharp/harp-operation-parser.y"
                                               {
            if (harp_operation_collocation_filter_new((yyvsp[-1].string_val), harp_collocation_left, -1, -1, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3499 "libharp/harp-operation-parser.c"
    break;

  case 121: /* operation: FUNC_COLLOCATE_LEFT '(' STRING_VALUE ',' int32_value ')'  */
#line 1043 "../harp/libharp/harp-operation-parser.y"
                                                               {
            if (harp_operation_collocation_filter_new((yyvsp[-3].string_val), harp_collocation_left, (yyvsp[-1].int32_val), -1, &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3512 "libharp/harp-operation-parser.c"
    break;

  case 122: /* operation: FUNC_COLLOCATE_LEFT '(' STRING_VALUE ',' int32_value ',' int32_value ')'  */
#line 1051 "../harp/libharp/harp-operation-parser.y"
                                                                               {
            if (harp_operation_collocation_filter_new((yyvsp[-5].string_val), harp_collocation_left, (yyvsp[-3].int32_val), (yyvsp[-1].int32_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-5].string_val));
                YYERROR;
            }
            free((yyvsp[-5].string_val));
        }
#line 3525 "libharp/harp-operation-parser.c"
    break;

  case 123: /* operation: FUNC_COLLOCATE_RIGHT '(' STRING_VALUE ')'  */
#line 1059 "../harp/libharp/harp-operation-parser.y"
                                                {
            if (harp_operation_collocation_filter_new((yyvsp[-1].string_val), harp_collocation_right, -1, -1, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3538 "libharp/harp-operation-parser.c"
    break;

  case 124: /* operation: FUNC_COLLOCATE_RIGHT '(' STRING_VALUE ',' int32_value ')'  */
#line 1067 "../harp/libharp/harp-operation-parser.y"
                                                                {
            if (harp_operation_collocation_filter_new((yyvsp[-3].string_val), harp_collocation_right, (yyvsp[-1].int32_val), -1, &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3551 "libharp/harp-operation-parser.c"
    break;

  case 125: /* operation: FUNC_COLLOCATE_RIGHT '(' STRING_VALUE ',' int32_value ',' int32_value ')'  */
#line 1075 "../harp/libharp/harp-operation-parser.y"
                                                                                {
            if (harp_operation_collocation_filter_new((yyvsp[-5].string_val), harp_collocation_right, (yyvsp[-3].int32_val), (yyvsp[-1].int32_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-5].string_val));
                YYERROR;
            }
            free((yyvsp[-5].string_val));
        }
#line 3564 "libharp/harp-operation-parser.c"
    break;

  case 126: /* operation: FUNC_DERIVE '(' identifier ')'  */
#line 1083 "../harp/libharp/harp-operation-parser.y"
                                     {
            /* even though it does nothing, we don't want this case to throw errors */
            /* it can also be used to perform an assert that a certain variable is available
             * (without having to perform a keep() with the full set of variables */
            if (harp_operation_derive_variable_new((yyvsp[-1].string_val), NULL, -1, NULL, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3580 "libharp/harp-operation-parser.c"
    break;

  case 127: /* operation: FUNC_DERIVE '(' identifier UNIT ')'  */
#line 1094 "../harp/libharp/harp-operation-parser.y"
                                          {
            if (harp_operation_derive_variable_new((yyvsp[-2].string_val), NULL, -1, NULL, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-2].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-2].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3595 "libharp/harp-operation-parser.c"
    break;

  case 128: /* operation: FUNC_DERIVE '(' identifier DATATYPE ')'  */
#line 1104 "../harp/libharp/harp-operation-parser.y"
                                              {
            harp_data_type data_type = (yyvsp[-1].int32_val);

            if (harp_operation_derive_variable_new((yyvsp[-2].string_val), &data_type, -1, NULL, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-2].string_val));
                YYERROR;
            }
            free((yyvsp[-2].string_val));
        }
#line 3610 "libharp/harp-operation-parser.c"
    break;

  case 129: /* operation: FUNC_DERIVE '(' identifier DATATYPE UNIT ')'  */
#line 1114 "../harp/libharp/harp-operation-parser.y"
                                                   {
            harp_data_type data_type = (yyvsp[-2].int32_val);

            if (harp_operation_derive_variable_new((yyvsp[-3].string_val), &data_type, -1, NULL, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3627 "libharp/harp-operation-parser.c"
    break;

  case 130: /* operation: FUNC_DERIVE '(' identifier dimensionspec ')'  */
#line 1126 "../harp/libharp/harp-operation-parser.y"
                                                   {
            if (harp_operation_derive_variable_new((yyvsp[-2].string_val), NULL, (yyvsp[-1].array)->num_elements, (yyvsp[-1].array)->array.int32_data, NULL,
                                                   &(yyval.operation)) != 0)
            {
                free((yyvsp[-2].string_val));
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            free((yyvsp[-2].string_val));
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 3643 "libharp/harp-operation-parser.c"
    break;

  case 131: /* operation: FUNC_DERIVE '(' identifier dimensionspec UNIT ')'  */
#line 1137 "../harp/libharp/harp-operation-parser.y"
                                                        {
            if (harp_operation_derive_variable_new((yyvsp[-3].string_val), NULL, (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.int32_data, (yyvsp[-1].string_val),
                                                   &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
            free((yyvsp[-1].string_val));
        }
#line 3661 "libharp/harp-operation-parser.c"
    break;

  case 132: /* operation: FUNC_DERIVE '(' identifier DATATYPE dimensionspec ')'  */
#line 1150 "../harp/libharp/harp-operation-parser.y"
                                                            {
            harp_data_type data_type = (yyvsp[-2].int32_val);

            if (harp_operation_derive_variable_new((yyvsp[-3].string_val), &data_type, (yyvsp[-1].array)->num_elements, (yyvsp[-1].array)->array.int32_data, NULL, &(yyval.operation)) !=
                0)
            {
                free((yyvsp[-3].string_val));
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 3679 "libharp/harp-operation-parser.c"
    break;

  case 133: /* operation: FUNC_DERIVE '(' identifier DATATYPE dimensionspec UNIT ')'  */
#line 1163 "../harp/libharp/harp-operation-parser.y"
                                                                 {
            harp_data_type data_type = (yyvsp[-3].int32_val);

            if (harp_operation_derive_variable_new((yyvsp[-4].string_val), &data_type, (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.int32_data, (yyvsp[-1].string_val), &(yyval.operation)) !=
                0)
            {
                free((yyvsp[-4].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
            free((yyvsp[-1].string_val));
        }
#line 3699 "libharp/harp-operation-parser.c"
    break;

  case 134: /* operation: FUNC_DERIVE_SMOOTHED_COLUMN '(' identifier dimensionspec UNIT ',' identifier UNIT ',' STRING_VALUE ',' ID_A ',' STRING_VALUE ')'  */
#line 1179 "../harp/libharp/harp-operation-parser.y"
                       {
            if (harp_operation_derive_smoothed_column_collocated_dataset_new((yyvsp[-12].string_val), (yyvsp[-11].array)->num_elements, (yyvsp[-11].array)->array.int32_data,
                                                                             (yyvsp[-10].string_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'a', (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-12].string_val));
                harp_sized_array_delete((yyvsp[-11].array));
                free((yyvsp[-10].string_val));
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-12].string_val));
            harp_sized_array_delete((yyvsp[-11].array));
            free((yyvsp[-10].string_val));
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3725 "libharp/harp-operation-parser.c"
    break;

  case 135: /* operation: FUNC_DERIVE_SMOOTHED_COLUMN '(' identifier dimensionspec UNIT ',' identifier UNIT ',' STRING_VALUE ',' ID_B ',' STRING_VALUE ')'  */
#line 1201 "../harp/libharp/harp-operation-parser.y"
                       {
            if (harp_operation_derive_smoothed_column_collocated_dataset_new((yyvsp[-12].string_val), (yyvsp[-11].array)->num_elements, (yyvsp[-11].array)->array.int32_data,
                                                                             (yyvsp[-10].string_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'b', (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-12].string_val));
                harp_sized_array_delete((yyvsp[-11].array));
                free((yyvsp[-10].string_val));
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-12].string_val));
            harp_sized_array_delete((yyvsp[-11].array));
            free((yyvsp[-10].string_val));
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3751 "libharp/harp-operation-parser.c"
    break;

  case 136: /* operation: FUNC_DERIVE_SMOOTHED_COLUMN '(' identifier dimensionspec UNIT ',' identifier UNIT ',' STRING_VALUE ')'  */
#line 1222 "../harp/libharp/harp-operation-parser.y"
                                                                                                             {
            if (harp_operation_derive_smoothed_column_collocated_product_new((yyvsp[-8].string_val), (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.int32_data,
                                                                             (yyvsp[-6].string_val), (yyvsp[-4].string_val), (yyvsp[-3].string_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-8].string_val));
                harp_sized_array_delete((yyvsp[-7].array));
                free((yyvsp[-6].string_val));
                free((yyvsp[-4].string_val));
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-8].string_val));
            harp_sized_array_delete((yyvsp[-7].array));
            free((yyvsp[-6].string_val));
            free((yyvsp[-4].string_val));
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3775 "libharp/harp-operation-parser.c"
    break;

  case 137: /* operation: FUNC_EXCLUDE '(' identifier_pattern_array ')'  */
#line 1241 "../harp/libharp/harp-operation-parser.y"
                                                    {
            if (harp_operation_exclude_variable_new((yyvsp[-1].array)->num_elements, (const char **)(yyvsp[-1].array)->array.string_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 3788 "libharp/harp-operation-parser.c"
    break;

  case 138: /* operation: FUNC_FLATTEN '(' DIMENSION ')'  */
#line 1249 "../harp/libharp/harp-operation-parser.y"
                                     {
            if (harp_operation_flatten_new((yyvsp[-1].int32_val), &(yyval.operation)) != 0) YYERROR;
        }
#line 3796 "libharp/harp-operation-parser.c"
    break;

  case 139: /* operation: FUNC_KEEP '(' identifier_pattern_array ')'  */
#line 1252 "../harp/libharp/harp-operation-parser.y"
                                                 {
            if (harp_operation_keep_variable_new((yyvsp[-1].array)->num_elements, (const char **)(yyvsp[-1].array)->array.string_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-1].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-1].array));
        }
#line 3809 "libharp/harp-operation-parser.c"
    break;

  case 140: /* operation: FUNC_LONGITUDE_RANGE '(' double_value ',' double_value ')'  */
#line 1260 "../harp/libharp/harp-operation-parser.y"
                                                                 {
            if (harp_operation_longitude_range_filter_new((yyvsp[-3].double_val), NULL, (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0) YYERROR;
        }
#line 3817 "libharp/harp-operation-parser.c"
    break;

  case 141: /* operation: FUNC_LONGITUDE_RANGE '(' double_value ',' double_value UNIT ')'  */
#line 1263 "../harp/libharp/harp-operation-parser.y"
                                                                      {
            if (harp_operation_longitude_range_filter_new((yyvsp[-4].double_val), NULL, (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3830 "libharp/harp-operation-parser.c"
    break;

  case 142: /* operation: FUNC_LONGITUDE_RANGE '(' double_value UNIT ',' double_value ')'  */
#line 1271 "../harp/libharp/harp-operation-parser.y"
                                                                      {
            if (harp_operation_longitude_range_filter_new((yyvsp[-4].double_val), (yyvsp[-3].string_val), (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3843 "libharp/harp-operation-parser.c"
    break;

  case 143: /* operation: FUNC_LONGITUDE_RANGE '(' double_value UNIT ',' double_value UNIT ')'  */
#line 1279 "../harp/libharp/harp-operation-parser.y"
                                                                           {
            if (harp_operation_longitude_range_filter_new((yyvsp[-5].double_val), (yyvsp[-4].string_val), (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-4].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3858 "libharp/harp-operation-parser.c"
    break;

  case 144: /* operation: FUNC_POINT_DISTANCE '(' double_value ',' double_value ',' double_value ')'  */
#line 1289 "../harp/libharp/harp-operation-parser.y"
                                                                                 {
            if (harp_operation_point_distance_filter_new((yyvsp[-5].double_val), NULL, (yyvsp[-3].double_val), NULL, (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0) YYERROR;
        }
#line 3866 "libharp/harp-operation-parser.c"
    break;

  case 145: /* operation: FUNC_POINT_DISTANCE '(' double_value ',' double_value ',' double_value UNIT ')'  */
#line 1292 "../harp/libharp/harp-operation-parser.y"
                                                                                      {
            if (harp_operation_point_distance_filter_new((yyvsp[-6].double_val), NULL, (yyvsp[-4].double_val), NULL, (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 3879 "libharp/harp-operation-parser.c"
    break;

  case 146: /* operation: FUNC_POINT_DISTANCE '(' double_value ',' double_value UNIT ',' double_value ')'  */
#line 1300 "../harp/libharp/harp-operation-parser.y"
                                                                                      {
            if (harp_operation_point_distance_filter_new((yyvsp[-6].double_val), NULL, (yyvsp[-4].double_val), (yyvsp[-3].string_val), (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
        }
#line 3892 "libharp/harp-operation-parser.c"
    break;

  case 147: /* operation: FUNC_POINT_DISTANCE '(' double_value ',' double_value UNIT ',' double_value UNIT ')'  */
#line 1308 "../harp/libharp/harp-operation-parser.y"
                                                                                           {
            if (harp_operation_point_distance_filter_new((yyvsp[-7].double_val), NULL, (yyvsp[-5].double_val), (yyvsp[-4].string_val), (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-4].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3907 "libharp/harp-operation-parser.c"
    break;

  case 148: /* operation: FUNC_POINT_DISTANCE '(' double_value UNIT ',' double_value ',' double_value ')'  */
#line 1318 "../harp/libharp/harp-operation-parser.y"
                                                                                      {
            if (harp_operation_point_distance_filter_new((yyvsp[-6].double_val), (yyvsp[-5].string_val), (yyvsp[-3].double_val), NULL, (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-5].string_val));
                YYERROR;
            }
            free((yyvsp[-5].string_val));
        }
#line 3920 "libharp/harp-operation-parser.c"
    break;

  case 149: /* operation: FUNC_POINT_DISTANCE '(' double_value UNIT ',' double_value ',' double_value UNIT ')'  */
#line 1326 "../harp/libharp/harp-operation-parser.y"
                                                                                           {
            if (harp_operation_point_distance_filter_new((yyvsp[-7].double_val), (yyvsp[-6].string_val), (yyvsp[-4].double_val), NULL, (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-6].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-6].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3935 "libharp/harp-operation-parser.c"
    break;

  case 150: /* operation: FUNC_POINT_DISTANCE '(' double_value UNIT ',' double_value UNIT ',' double_value ')'  */
#line 1336 "../harp/libharp/harp-operation-parser.y"
                                                                                           {
            if (harp_operation_point_distance_filter_new((yyvsp[-7].double_val), (yyvsp[-6].string_val), (yyvsp[-4].double_val), (yyvsp[-3].string_val), (yyvsp[-1].double_val), NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-6].string_val));
                free((yyvsp[-3].string_val));
                YYERROR;
            }
            free((yyvsp[-6].string_val));
            free((yyvsp[-3].string_val));
        }
#line 3950 "libharp/harp-operation-parser.c"
    break;

  case 151: /* operation: FUNC_POINT_DISTANCE '(' double_value UNIT ',' double_value UNIT ',' double_value UNIT ')'  */
#line 1346 "../harp/libharp/harp-operation-parser.y"
                                                                                                {
            if (harp_operation_point_distance_filter_new((yyvsp[-8].double_val), (yyvsp[-7].string_val), (yyvsp[-5].double_val), (yyvsp[-4].string_val), (yyvsp[-2].double_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-7].string_val));
                free((yyvsp[-4].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-7].string_val));
            free((yyvsp[-4].string_val));
            free((yyvsp[-1].string_val));
        }
#line 3967 "libharp/harp-operation-parser.c"
    break;

  case 152: /* operation: FUNC_POINT_IN_AREA '(' '(' double_array ')' ',' '(' double_array ')' ')'  */
#line 1358 "../harp/libharp/harp-operation-parser.y"
                                                                               {
            if (harp_operation_point_in_area_filter_new(NULL, (yyvsp[-6].array)->num_elements, (yyvsp[-6].array)->array.double_data, NULL,
                                                        (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-6].array));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-6].array));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 3983 "libharp/harp-operation-parser.c"
    break;

  case 153: /* operation: FUNC_POINT_IN_AREA '(' '(' double_array ')' ',' '(' double_array ')' UNIT ')'  */
#line 1369 "../harp/libharp/harp-operation-parser.y"
                                                                                    {
            if (harp_operation_point_in_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, NULL,
                                                        (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 4001 "libharp/harp-operation-parser.c"
    break;

  case 154: /* operation: FUNC_POINT_IN_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' ')'  */
#line 1382 "../harp/libharp/harp-operation-parser.y"
                                                                                    {
            if (harp_operation_point_in_area_filter_new(NULL, (yyvsp[-7].array)->num_elements, (yyvsp[-7].array)->array.double_data, (yyvsp[-5].string_val),
                                                        (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-7].array));
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-7].array));
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 4019 "libharp/harp-operation-parser.c"
    break;

  case 155: /* operation: FUNC_POINT_IN_AREA '(' '(' double_array ')' UNIT ',' '(' double_array ')' UNIT ')'  */
#line 1395 "../harp/libharp/harp-operation-parser.y"
                                                                                         {
            if (harp_operation_point_in_area_filter_new(NULL, (yyvsp[-8].array)->num_elements, (yyvsp[-8].array)->array.double_data, (yyvsp[-6].string_val),
                                                        (yyvsp[-3].array)->num_elements, (yyvsp[-3].array)->array.double_data, (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-8].array));
                free((yyvsp[-6].string_val));
                harp_sized_array_delete((yyvsp[-3].array));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-8].array));
            free((yyvsp[-6].string_val));
            harp_sized_array_delete((yyvsp[-3].array));
            free((yyvsp[-1].string_val));
        }
#line 4039 "libharp/harp-operation-parser.c"
    break;

  case 156: /* operation: FUNC_POINT_IN_AREA '(' STRING_VALUE ')'  */
#line 1410 "../harp/libharp/harp-operation-parser.y"
                                              {
            if (harp_operation_point_in_area_filter_new((yyvsp[-1].string_val), 0, NULL, NULL, 0, NULL, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 4052 "libharp/harp-operation-parser.c"
    break;

  case 157: /* operation: FUNC_REBIN '(' DIMENSION ',' identifier UNIT ',' '(' double_array ')' ')'  */
#line 1418 "../harp/libharp/harp-operation-parser.y"
                                                                                {
            if (harp_operation_rebin_new((yyvsp[-8].int32_val), (yyvsp[-6].string_val), (yyvsp[-5].string_val), (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, &(yyval.operation)) != 0)
            {
                free((yyvsp[-6].string_val));
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            free((yyvsp[-6].string_val));
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 4069 "libharp/harp-operation-parser.c"
    break;

  case 158: /* operation: FUNC_REBIN '(' DIMENSION ',' identifier UNIT ',' int32_value ',' double_value ',' double_value ')'  */
#line 1430 "../harp/libharp/harp-operation-parser.y"
                                                                                                         {
            harp_sized_array *array;
            long i;

            if (harp_sized_array_new(harp_type_double, &array) != 0)
            {
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                YYERROR;
            }
            for (i = 0; i < (yyvsp[-5].int32_val); i++)
            {
                if (harp_sized_array_add_double(array, (yyvsp[-3].double_val) + i * (yyvsp[-1].double_val)) != 0)
                {
                    harp_sized_array_delete(array);
                    free((yyvsp[-8].string_val));
                    free((yyvsp[-7].string_val));
                    YYERROR;
                }
            }
            if (harp_operation_rebin_new((yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), array->num_elements, array->array.double_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete(array);
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                YYERROR;
            }
            harp_sized_array_delete(array);
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
        }
#line 4105 "libharp/harp-operation-parser.c"
    break;

  case 159: /* operation: FUNC_REGRID '(' DIMENSION ',' identifier UNIT ',' '(' double_array ')' ')'  */
#line 1461 "../harp/libharp/harp-operation-parser.y"
                                                                                 {
            if (harp_operation_regrid_new((yyvsp[-8].int32_val), (yyvsp[-6].string_val), (yyvsp[-5].string_val), (yyvsp[-2].array)->num_elements, (yyvsp[-2].array)->array.double_data, 0, NULL, &(yyval.operation)) != 0)
            {
                free((yyvsp[-6].string_val));
                free((yyvsp[-5].string_val));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            free((yyvsp[-6].string_val));
            free((yyvsp[-5].string_val));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 4122 "libharp/harp-operation-parser.c"
    break;

  case 160: /* operation: FUNC_REGRID '(' DIMENSION ',' identifier UNIT ',' '(' double_array ')' ',' '(' double_array ')' ')'  */
#line 1473 "../harp/libharp/harp-operation-parser.y"
                                                                                                          {
            if (harp_operation_regrid_new((yyvsp[-12].int32_val), (yyvsp[-10].string_val), (yyvsp[-9].string_val), (yyvsp[-6].array)->num_elements, (yyvsp[-6].array)->array.double_data, (yyvsp[-2].array)->num_elements,
                                          (yyvsp[-2].array)->array.double_data, &(yyval.operation)) != 0)
            {
                free((yyvsp[-10].string_val));
                free((yyvsp[-9].string_val));
                harp_sized_array_delete((yyvsp[-6].array));
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            free((yyvsp[-10].string_val));
            free((yyvsp[-9].string_val));
            harp_sized_array_delete((yyvsp[-6].array));
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 4142 "libharp/harp-operation-parser.c"
    break;

  case 161: /* operation: FUNC_REGRID '(' DIMENSION ',' identifier UNIT ',' int32_value ',' double_value ',' double_value ')'  */
#line 1488 "../harp/libharp/harp-operation-parser.y"
                                                                                                          {
            harp_sized_array *array;
            long i;

            if (harp_sized_array_new(harp_type_double, &array) != 0)
            {
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                YYERROR;
            }
            for (i = 0; i < (yyvsp[-5].int32_val); i++)
            {
                if (harp_sized_array_add_double(array, (yyvsp[-3].double_val) + i * (yyvsp[-1].double_val)) != 0)
                {
                    harp_sized_array_delete(array);
                    free((yyvsp[-8].string_val));
                    free((yyvsp[-7].string_val));
                    YYERROR;
                }
            }
            if (harp_operation_regrid_new((yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), array->num_elements, array->array.double_data, 0, NULL, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete(array);
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                YYERROR;
            }
            harp_sized_array_delete(array);
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
        }
#line 4178 "libharp/harp-operation-parser.c"
    break;

  case 162: /* operation: FUNC_REGRID '(' DIMENSION ',' identifier UNIT ',' STRING_VALUE ',' ID_A ',' STRING_VALUE ')'  */
#line 1519 "../harp/libharp/harp-operation-parser.y"
                                                                                                   {
            if (harp_operation_regrid_collocated_dataset_new((yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'a', (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4197 "libharp/harp-operation-parser.c"
    break;

  case 163: /* operation: FUNC_REGRID '(' DIMENSION ',' identifier UNIT ',' STRING_VALUE ',' ID_B ',' STRING_VALUE ')'  */
#line 1533 "../harp/libharp/harp-operation-parser.y"
                                                                                                   {
            if (harp_operation_regrid_collocated_dataset_new((yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'b', (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4216 "libharp/harp-operation-parser.c"
    break;

  case 164: /* operation: FUNC_REGRID '(' DIMENSION ',' identifier UNIT ',' STRING_VALUE ')'  */
#line 1547 "../harp/libharp/harp-operation-parser.y"
                                                                         {
            if (harp_operation_regrid_collocated_product_new((yyvsp[-6].int32_val), (yyvsp[-4].string_val), (yyvsp[-3].string_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-4].string_val));
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-4].string_val));
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4233 "libharp/harp-operation-parser.c"
    break;

  case 165: /* operation: FUNC_RENAME '(' identifier ',' identifier ')'  */
#line 1559 "../harp/libharp/harp-operation-parser.y"
                                                    {
            if (harp_operation_rename_new((yyvsp[-3].string_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4248 "libharp/harp-operation-parser.c"
    break;

  case 166: /* operation: FUNC_SET '(' STRING_VALUE ',' STRING_VALUE ')'  */
#line 1569 "../harp/libharp/harp-operation-parser.y"
                                                     {
            if (harp_operation_set_new((yyvsp[-3].string_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4263 "libharp/harp-operation-parser.c"
    break;

  case 167: /* operation: FUNC_SMOOTH '(' identifier ',' DIMENSION ',' identifier UNIT ',' STRING_VALUE ',' ID_A ',' STRING_VALUE ')'  */
#line 1579 "../harp/libharp/harp-operation-parser.y"
                                                                                                                  {
            if (harp_operation_smooth_collocated_dataset_new(1, (const char **)&(yyvsp[-12].string_val), (yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'a', (yyvsp[-1].string_val), &(yyval.operation)) !=
                0)
            {
                free((yyvsp[-12].string_val));
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-12].string_val));
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4285 "libharp/harp-operation-parser.c"
    break;

  case 168: /* operation: FUNC_SMOOTH '(' identifier ',' DIMENSION ',' identifier UNIT ',' STRING_VALUE ',' ID_B ',' STRING_VALUE ')'  */
#line 1596 "../harp/libharp/harp-operation-parser.y"
                                                                                                                  {
            if (harp_operation_smooth_collocated_dataset_new(1, (const char **)&(yyvsp[-12].string_val), (yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'b', (yyvsp[-1].string_val), &(yyval.operation)) !=
                0)
            {
                free((yyvsp[-12].string_val));
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-12].string_val));
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4307 "libharp/harp-operation-parser.c"
    break;

  case 169: /* operation: FUNC_SMOOTH '(' identifier ',' DIMENSION ',' identifier UNIT ',' STRING_VALUE ')'  */
#line 1613 "../harp/libharp/harp-operation-parser.y"
                                                                                        {
            if (harp_operation_smooth_collocated_product_new(1, (const char **)&(yyvsp[-8].string_val), (yyvsp[-6].int32_val), (yyvsp[-4].string_val), (yyvsp[-3].string_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-8].string_val));
                free((yyvsp[-4].string_val));
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-8].string_val));
            free((yyvsp[-4].string_val));
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4326 "libharp/harp-operation-parser.c"
    break;

  case 170: /* operation: FUNC_SMOOTH '(' '(' identifier_array ')' ',' DIMENSION ',' identifier UNIT ',' STRING_VALUE ',' ID_A ',' STRING_VALUE ')'  */
#line 1628 "../harp/libharp/harp-operation-parser.y"
                       {
            if (harp_operation_smooth_collocated_dataset_new((yyvsp[-13].array)->num_elements, (const char **)(yyvsp[-13].array)->array.string_data,
                                                     (yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'a', (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-13].array));
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-13].array));
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4348 "libharp/harp-operation-parser.c"
    break;

  case 171: /* operation: FUNC_SMOOTH '(' '(' identifier_array ')' ',' DIMENSION ',' identifier UNIT ',' STRING_VALUE ',' ID_B ',' STRING_VALUE ')'  */
#line 1646 "../harp/libharp/harp-operation-parser.y"
                       {
            if (harp_operation_smooth_collocated_dataset_new((yyvsp[-13].array)->num_elements, (const char **)(yyvsp[-13].array)->array.string_data,
                                                     (yyvsp[-10].int32_val), (yyvsp[-8].string_val), (yyvsp[-7].string_val), (yyvsp[-5].string_val), 'b', (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-13].array));
                free((yyvsp[-8].string_val));
                free((yyvsp[-7].string_val));
                free((yyvsp[-5].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-13].array));
            free((yyvsp[-8].string_val));
            free((yyvsp[-7].string_val));
            free((yyvsp[-5].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4370 "libharp/harp-operation-parser.c"
    break;

  case 172: /* operation: FUNC_SMOOTH '(' '(' identifier_array ')' ',' DIMENSION ',' identifier UNIT ',' STRING_VALUE ')'  */
#line 1663 "../harp/libharp/harp-operation-parser.y"
                                                                                                      {
            if (harp_operation_smooth_collocated_product_new((yyvsp[-9].array)->num_elements, (const char **)(yyvsp[-9].array)->array.string_data,
                                                             (yyvsp[-6].int32_val), (yyvsp[-4].string_val), (yyvsp[-3].string_val), (yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-9].array));
                free((yyvsp[-4].string_val));
                free((yyvsp[-3].string_val));
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-9].array));
            free((yyvsp[-4].string_val));
            free((yyvsp[-3].string_val));
            free((yyvsp[-1].string_val));
        }
#line 4390 "libharp/harp-operation-parser.c"
    break;

  case 173: /* operation: FUNC_SORT '(' identifier ')'  */
#line 1678 "../harp/libharp/harp-operation-parser.y"
                                   {
            if (harp_operation_sort_new(1, (const char **)&(yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 4403 "libharp/harp-operation-parser.c"
    break;

  case 174: /* operation: FUNC_SORT '(' '(' identifier_array ')' ')'  */
#line 1686 "../harp/libharp/harp-operation-parser.y"
                                                 {
            if (harp_operation_sort_new((yyvsp[-2].array)->num_elements, (const char **)(yyvsp[-2].array)->array.string_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 4416 "libharp/harp-operation-parser.c"
    break;

  case 175: /* operation: FUNC_SQUASH '(' DIMENSION ',' identifier ')'  */
#line 1694 "../harp/libharp/harp-operation-parser.y"
                                                   {
            if (harp_operation_squash_new((yyvsp[-3].int32_val), 1, (const char **)&(yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 4429 "libharp/harp-operation-parser.c"
    break;

  case 176: /* operation: FUNC_SQUASH '(' DIMENSION ',' '(' identifier_array ')' ')'  */
#line 1702 "../harp/libharp/harp-operation-parser.y"
                                                                 {
            if (harp_operation_squash_new((yyvsp[-5].int32_val), (yyvsp[-2].array)->num_elements, (const char **)(yyvsp[-2].array)->array.string_data, &(yyval.operation)) != 0)
            {
                harp_sized_array_delete((yyvsp[-2].array));
                YYERROR;
            }
            harp_sized_array_delete((yyvsp[-2].array));
        }
#line 4442 "libharp/harp-operation-parser.c"
    break;

  case 177: /* operation: FUNC_VALID '(' identifier ')'  */
#line 1710 "../harp/libharp/harp-operation-parser.y"
                                    {
            if (harp_operation_valid_range_filter_new((yyvsp[-1].string_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-1].string_val));
                YYERROR;
            }
            free((yyvsp[-1].string_val));
        }
#line 4455 "libharp/harp-operation-parser.c"
    break;

  case 178: /* operation: FUNC_WRAP '(' identifier ',' double_value ',' double_value ')'  */
#line 1718 "../harp/libharp/harp-operation-parser.y"
                                                                     {
            if (harp_operation_wrap_new((yyvsp[-5].string_val), NULL, (yyvsp[-3].double_val), (yyvsp[-1].double_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-5].string_val));
                YYERROR;
            }
            free((yyvsp[-5].string_val));
        }
#line 4468 "libharp/harp-operation-parser.c"
    break;

  case 179: /* operation: FUNC_WRAP '(' identifier UNIT ',' double_value ',' double_value ')'  */
#line 1726 "../harp/libharp/harp-operation-parser.y"
                                                                          {
            if (harp_operation_wrap_new((yyvsp[-6].string_val), (yyvsp[-5].string_val), (yyvsp[-3].double_val), (yyvsp[-1].double_val), &(yyval.operation)) != 0)
            {
                free((yyvsp[-6].string_val));
                free((yyvsp[-5].string_val));
                YYERROR;
            }
            free((yyvsp[-6].string_val));
            free((yyvsp[-5].string_val));
        }
#line 4483 "libharp/harp-operation-parser.c"
    break;

  case 180: /* program: program ';' operation  */
#line 1739 "../harp/libharp/harp-operation-parser.y"
                            {
            if (harp_program_add_operation((yyvsp[-2].program), (yyvsp[0].operation)) != 0)
            {
                harp_program_delete((yyvsp[-2].program));
                harp_operation_delete((yyvsp[0].operation));
                YYERROR;
            }
            (yyval.program) = (yyvsp[-2].program);
        }
#line 4497 "libharp/harp-operation-parser.c"
    break;

  case 181: /* program: operation  */
#line 1748 "../harp/libharp/harp-operation-parser.y"
                {
            if (harp_program_new(&(yyval.program)) != 0)
            {
                harp_operation_delete((yyvsp[0].operation));
                YYERROR;
            }
            if (harp_program_add_operation((yyval.program), (yyvsp[0].operation)) != 0)
            {
                harp_operation_delete((yyvsp[0].operation));
                harp_program_delete((yyval.program));
                YYERROR;
            }
        }
#line 4515 "libharp/harp-operation-parser.c"
    break;

  case 182: /* program: %empty  */
#line 1761 "../harp/libharp/harp-operation-parser.y"
                  {
            if (harp_program_new(&(yyval.program)) != 0) YYERROR;
        }
#line 4523 "libharp/harp-operation-parser.c"
    break;


#line 4527 "libharp/harp-operation-parser.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yytoken};
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == -1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (yymsg)
              {
                yysyntax_error_status
                  = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
                yymsgp = yymsg;
              }
            else
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = YYENOMEM;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == YYENOMEM)
          goto yyexhaustedlab;
      }
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if 1
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;
#endif


/*-------------------------------------------------------.
| yyreturn -- parsing is finished, clean up and return.  |
`-------------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
  return yyresult;
}

#line 1766 "../harp/libharp/harp-operation-parser.y"


/* *INDENT-ON* */

int harp_program_from_string(const char *str, harp_program **program)
{
    void *bufstate;

    /* if this doesn't hold we need to introduce a separate harp_sized_array for enums */
    assert(sizeof(int32_t) == sizeof(harp_dimension_type));

    harp_errno = 0;
    parsed_program = NULL;
    bufstate = (void *)harp_operation_parser__scan_string(str);
    if (harp_operation_parser_parse() != 0)
    {
        if (parsed_program != NULL)
        {
            harp_program_delete(parsed_program);
        }
        if (harp_errno == 0)
        {
            harp_set_error(HARP_ERROR_OPERATION_SYNTAX, NULL);
        }
        harp_operation_parser__delete_buffer(bufstate);
        return -1;
    }
    harp_operation_parser__delete_buffer(bufstate);
    *program = parsed_program;

    return 0;
}
