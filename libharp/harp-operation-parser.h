/* A Bison parser, made by GNU Bison 3.7.5.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_LIBHARP_HARP_OPERATION_PARSER_H_INCLUDED
# define YY_YY_LIBHARP_HARP_OPERATION_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    NAME = 258,                    /* NAME  */
    NAME_PATTERN = 259,            /* NAME_PATTERN  */
    STRING_VALUE = 260,            /* STRING_VALUE  */
    INTEGER_VALUE = 261,           /* INTEGER_VALUE  */
    DOUBLE_VALUE = 262,            /* DOUBLE_VALUE  */
    DATATYPE = 263,                /* DATATYPE  */
    DIMENSION = 264,               /* DIMENSION  */
    UNIT = 265,                    /* UNIT  */
    ID_A = 266,                    /* ID_A  */
    ID_B = 267,                    /* ID_B  */
    FUNC_AREA_COVERS_AREA = 268,   /* FUNC_AREA_COVERS_AREA  */
    FUNC_AREA_COVERS_POINT = 269,  /* FUNC_AREA_COVERS_POINT  */
    FUNC_AREA_INSIDE_AREA = 270,   /* FUNC_AREA_INSIDE_AREA  */
    FUNC_AREA_INTERSECTS_AREA = 271, /* FUNC_AREA_INTERSECTS_AREA  */
    FUNC_BIN = 272,                /* FUNC_BIN  */
    FUNC_BIN_SPATIAL = 273,        /* FUNC_BIN_SPATIAL  */
    FUNC_CLAMP = 274,              /* FUNC_CLAMP  */
    FUNC_COLLOCATE_LEFT = 275,     /* FUNC_COLLOCATE_LEFT  */
    FUNC_COLLOCATE_RIGHT = 276,    /* FUNC_COLLOCATE_RIGHT  */
    FUNC_DERIVE = 277,             /* FUNC_DERIVE  */
    FUNC_DERIVE_SMOOTHED_COLUMN = 278, /* FUNC_DERIVE_SMOOTHED_COLUMN  */
    FUNC_EXCLUDE = 279,            /* FUNC_EXCLUDE  */
    FUNC_FLATTEN = 280,            /* FUNC_FLATTEN  */
    FUNC_INDEX = 281,              /* FUNC_INDEX  */
    FUNC_KEEP = 282,               /* FUNC_KEEP  */
    FUNC_LONGITUDE_RANGE = 283,    /* FUNC_LONGITUDE_RANGE  */
    FUNC_POINT_DISTANCE = 284,     /* FUNC_POINT_DISTANCE  */
    FUNC_POINT_IN_AREA = 285,      /* FUNC_POINT_IN_AREA  */
    FUNC_REBIN = 286,              /* FUNC_REBIN  */
    FUNC_REGRID = 287,             /* FUNC_REGRID  */
    FUNC_RENAME = 288,             /* FUNC_RENAME  */
    FUNC_SET = 289,                /* FUNC_SET  */
    FUNC_SMOOTH = 290,             /* FUNC_SMOOTH  */
    FUNC_SORT = 291,               /* FUNC_SORT  */
    FUNC_SQUASH = 292,             /* FUNC_SQUASH  */
    FUNC_VALID = 293,              /* FUNC_VALID  */
    FUNC_WRAP = 294,               /* FUNC_WRAP  */
    NAN = 295,                     /* NAN  */
    INF = 296,                     /* INF  */
    IN = 297,                      /* IN  */
    EQUAL = 298,                   /* EQUAL  */
    NOT_EQUAL = 299,               /* NOT_EQUAL  */
    GREATER_EQUAL = 300,           /* GREATER_EQUAL  */
    LESS_EQUAL = 301,              /* LESS_EQUAL  */
    BIT_NAND = 302,                /* BIT_NAND  */
    BIT_AND = 303,                 /* BIT_AND  */
    NOT = 304                      /* NOT  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define NAME 258
#define NAME_PATTERN 259
#define STRING_VALUE 260
#define INTEGER_VALUE 261
#define DOUBLE_VALUE 262
#define DATATYPE 263
#define DIMENSION 264
#define UNIT 265
#define ID_A 266
#define ID_B 267
#define FUNC_AREA_COVERS_AREA 268
#define FUNC_AREA_COVERS_POINT 269
#define FUNC_AREA_INSIDE_AREA 270
#define FUNC_AREA_INTERSECTS_AREA 271
#define FUNC_BIN 272
#define FUNC_BIN_SPATIAL 273
#define FUNC_CLAMP 274
#define FUNC_COLLOCATE_LEFT 275
#define FUNC_COLLOCATE_RIGHT 276
#define FUNC_DERIVE 277
#define FUNC_DERIVE_SMOOTHED_COLUMN 278
#define FUNC_EXCLUDE 279
#define FUNC_FLATTEN 280
#define FUNC_INDEX 281
#define FUNC_KEEP 282
#define FUNC_LONGITUDE_RANGE 283
#define FUNC_POINT_DISTANCE 284
#define FUNC_POINT_IN_AREA 285
#define FUNC_REBIN 286
#define FUNC_REGRID 287
#define FUNC_RENAME 288
#define FUNC_SET 289
#define FUNC_SMOOTH 290
#define FUNC_SORT 291
#define FUNC_SQUASH 292
#define FUNC_VALID 293
#define FUNC_WRAP 294
#define NAN 295
#define INF 296
#define IN 297
#define EQUAL 298
#define NOT_EQUAL 299
#define GREATER_EQUAL 300
#define LESS_EQUAL 301
#define BIT_NAND 302
#define BIT_AND 303
#define NOT 304

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 219 "../harp/libharp/harp-operation-parser.y"

    double double_val;
    int32_t int32_val;
    char *string_val;
    const char *const_string_val;
    harp_operation *operation;
    harp_sized_array *array;
    harp_program *program;

    harp_comparison_operator_type comparison_operator;
    harp_bit_mask_operator_type bit_mask_operator;
    harp_membership_operator_type membership_operator;

#line 179 "libharp/harp-operation-parser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_LIBHARP_HARP_OPERATION_PARSER_H_INCLUDED  */
